% created on 2018/08/07 for GPS project by Zahra Ahmadi
% This code models inosphere erroe by using Klobuchar method. 


clc 
clear
format longg
fid=fopen('tehn0010.18o');
for i=1:15
    d=fgetl(fid);
    if i==15
      Rrecev=str2num(d(3:60));
    end 
end
fid=fopen('tehn0010.18n');
for i=1:12
    d=fgetl(fid);
    if i==5 
        alfa=str2num(d(1:60));
    elseif i==6
        beta=str2num(d(1:60));
    elseif i==12
      gpst=str2num(d(1:28));
    end 
end
spheroid = referenceEllipsoid('WGS 84');
sat=textread('18sat.txt');
l=1;
for i=1:10
[Rmonhani(1,1),Rmonhani(1,2),Rmonhani(1,3)]=ecef2geodetic(spheroid,sat(i,1),sat(i,2),sat(i,3));
[Rmonhani(2,1),Rmonhani(2,2),Rmonhani(2,3)]=ecef2geodetic(spheroid,Rrecev(1),Rrecev(2),Rrecev(3));
[az,elev,slan]=geodetic2aer(Rmonhani(2,1),Rmonhani(2,2),Rmonhani(2,3),Rmonhani(1,1),Rmonhani(1,2),Rmonhani(1,3),spheroid);
A=az*0.0055555555555;
E=elev*0.0055555555555;
fisem=Rmonhani(2,1)*0.0055555555555;
lasem=Rmonhani(2,2)*0.0055555555555;
sai=(0.0137/(E+0.11))-0.022;
fiI= fisem+sai*cosd(az);

if fiI>0.416 
    fiI=0.416;
elseif fiI< -0.416
    fiI=-0.416;
end
laI=lasem+(sai*sind(az)/cosd(fiI/0.0055555555555));
fiM=fiI+0.064*cosd((laI-1.617)/0.0055555555555);
t=43200*laI+gpst+(i-1)*30;
if t>86400
    t=t-86400;
elseif t< 0
    t=t+86400;
end
F=1+16*(0.53-E)^3;
P=beta(1)+beta(2)*fiM+beta(3)*fiM^2+beta(4)*fiM^3;
a=alfa(1)+alfa(2)*fiM+alfa(3)*fiM^2+alfa(4)*fiM^3;
X=2*pi*(t-50400)/P;
Tion=F*(5*10^(-9)+a*(1-X^2/2+X^4/24));
data(l,:)=[18,gpst+(l-1)*30,Tion];
l=l+1;
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
fid=fopen('tehn0010.18n');
for i=1:12
    d=fgetl(fid);
  if i==20
      gpst=str2num(d(1:28));
    end 
end
sat=textread('29sat.txt');
for i=1:10
[Rmonhani(1,1),Rmonhani(1,2),Rmonhani(1,3)]=ecef2geodetic(spheroid,sat(i,1),sat(i,2),sat(i,3));
[Rmonhani(2,1),Rmonhani(2,2),Rmonhani(2,3)]=ecef2geodetic(spheroid,Rrecev(1),Rrecev(2),Rrecev(3));
[az,elev,slan]=geodetic2aer(Rmonhani(2,1),Rmonhani(2,2),Rmonhani(2,3),Rmonhani(1,1),Rmonhani(1,2),Rmonhani(1,3),spheroid);
A=az*0.0055555555555;
E=elev*0.0055555555555;
fisem=Rmonhani(2,1)*0.0055555555555;
lasem=Rmonhani(2,2)*0.0055555555555;
sai=(0.0137/(E+0.11))-0.022;
fiI= fisem+sai*cosd(az);

if fiI>0.416 
    fiI=0.416;
elseif fiI< -0.416
    fiI=-0.416;
end
laI=lasem+(sai*sind(az)/cosd(fiI/0.0055555555555));
fiM=fiI+0.064*cosd((laI-1.617)/0.0055555555555);
t=43200*laI+gpst+(i-1)*30;
if t>86400
    t=t-86400;
elseif t< 0
    t=t+86400;
end
F=1+16*(0.53-E)^3;
P=beta(1)+beta(2)*fiM+beta(3)*fiM^2+beta(4)*fiM^3;
a=alfa(1)+alfa(2)*fiM+alfa(3)*fiM^2+alfa(4)*fiM^3;
X=2*pi*(t-50400)/P;
Tion=F*(5*10^(-9)+a*(1-X^2/2+X^4/24));
data(l,:)=[29,gpst+(l-1)*30,Tion];
l=l+1;
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
fid=fopen('tehn0010.18n');
for i=1:12
    d=fgetl(fid);
  if i==20
      gpst=str2num(d(1:28));
    end 
end
sat=textread('26sat.txt');
for i=1:10
[Rmonhani(1,1),Rmonhani(1,2),Rmonhani(1,3)]=ecef2geodetic(spheroid,sat(i,1),sat(i,2),sat(i,3));
[Rmonhani(2,1),Rmonhani(2,2),Rmonhani(2,3)]=ecef2geodetic(spheroid,Rrecev(1),Rrecev(2),Rrecev(3));
[az,elev,slan]=geodetic2aer(Rmonhani(2,1),Rmonhani(2,2),Rmonhani(2,3),Rmonhani(1,1),Rmonhani(1,2),Rmonhani(1,3),spheroid);
A=az*0.0055555555555;
E=elev*0.0055555555555;
fisem=Rmonhani(2,1)*0.0055555555555;
lasem=Rmonhani(2,2)*0.0055555555555;
sai=(0.0137/(E+0.11))-0.022;
fiI= fisem+sai*cosd(az);

if fiI>0.416 
    fiI=0.416;
elseif fiI< -0.416
    fiI=-0.416;
end
laI=lasem+(sai*sind(az)/cosd(fiI/0.0055555555555));
fiM=fiI+0.064*cosd((laI-1.617)/0.0055555555555);
t=43200*laI+gpst+(i-1)*30;
if t>86400
    t=t-86400;
elseif t< 0
    t=t+86400;
end
F=1+16*(0.53-E)^3;
P=beta(1)+beta(2)*fiM+beta(3)*fiM^2+beta(4)*fiM^3;
a=alfa(1)+alfa(2)*fiM+alfa(3)*fiM^2+alfa(4)*fiM^3;
X=2*pi*(t-50400)/P;
Tion=F*(5*10^(-9)+a*(1-X^2/2+X^4/24));
data(l,:)=[26,gpst+(l-1)*30,Tion];
l=l+1;
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
figure
plot([data(1,1),data(11,1),data(21,1)],[data(1,3),data(11,3),data(21,3)])
figure
plot(data(1:10,1),data(1:10,3))
