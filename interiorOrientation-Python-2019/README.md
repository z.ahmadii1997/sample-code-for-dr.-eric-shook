Internal orientation means converting a device coordinate system to a photographic coordinate system. We have 4 files in this project.

item1:
This is the main file in which the program runs, the image coordinates are loaded, the conversion type is specified, and the device coordinates are entered. Finally, all other functions are invoked.

Convert file:
  In this file, there are 4 functions for converting Conformal, Efine, Projective, and Polynomial. The matrices L, A are obtained according to the photographic and device coordinates and the number of control points.

RMSE file:
 In this file, there are 4 functions for calculating the remaining vector, RMSE conversion, and Lprim. We take the Lprim matrix and A, by taking the device coordinates of the left and right images and the number of points and conversion parameters. Then, after subtracting Lprim from the matrix L (device coordinates), the remaining vector is obtained. Finally, we also calculate the RMSE.

Save file:
In this file, there is a function to save the conversion type, conversion parameters, the remaining vector of check and control points as well as RMSE check and checkpoints in the text file.

PlotV file:
 This file contains a function to display the conversion type, device coordinates, Lprim control and check, and the number of control and checkpoints.

How to use:
Choose one model : 
1-Conformal
2-Affine
3-Projective
4-polynomial
2
1-From Picture 
2-Saved Data2
1-Left 
2-Righti1
Enter Check Point Numebr (type 'end' to exit): 1
Enter Check Point Numebr (type 'end' to exit): 4
Enter Check Point Numebr_(type 'end' to exit): end
