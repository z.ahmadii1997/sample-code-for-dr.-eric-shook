# -*- coding: utf-8 -*-
"""
Created on Sun Oct 27 20:45:42 2019

@author: Zahra Ahmadi

This will be invoked by item1.

In this file, there are 4 functions for calculating the remaining vector, RMSE conversion and Lprim. We take the Lprim matrix and A, by taking the device coordinates of the left and right images and the number of points and conversion parameters. Then, after subtracting Lprim from the matrix L (device coordinates), the remaining vector is obtained. Finally, we also calculate the RMSE.
"""

import numpy as np
from numpy.linalg import norm


# ----------------------------------------------- Conformal ------------------------------------
def V_Conformal(xyPointPic1,xyPointPic2,pointNumber,conformalParams):
    if len(pointNumber )== 1:
        n = 0
    else:
        n = 1
    A = np.matrix(np.zeros((2*len(pointNumber),4)))
    k=0
    for i in range(len(pointNumber)):
        A[k,:]=[xyPointPic2[pointNumber[i],0],-1*xyPointPic2[pointNumber[i],1],1,0]
        A[k+1,:]=[xyPointPic2[pointNumber[i],1],xyPointPic2[pointNumber[i],0],0,1]
        k+= 2 
    Lprim = A * conformalParams
    vConformal = np.matrix(np.zeros((2*len(pointNumber),1)))
    k=0
    for i in range(len(pointNumber)):
        vConformal[k] = xyPointPic1[pointNumber[i],0] - Lprim[k]
        vConformal[k+1] = xyPointPic1[pointNumber[i],1] - Lprim[k+1]
        k+=2
    RMSE_Conformal = norm(vConformal)/(len(pointNumber)-n)
    return RMSE_Conformal,vConformal,Lprim

# ----------------------------------------------- Affine ------------------------------------
def V_Affine(xyPointPic1,xyPointPic2,pointNumber,affineParams):
    if len(pointNumber )== 1:
        n = 0
    else:
        n = 1
    A = np.matrix(np.zeros((2*len(pointNumber),6)))
    k=0
    for i in range(len(pointNumber)):
        A[k,:]=[xyPointPic2[pointNumber[i],0],xyPointPic2[pointNumber[i],1],1,0,0,0]
        A[k+1,:]=[0,0,0,xyPointPic2[pointNumber[i],0],xyPointPic2[pointNumber[i],1],1]
        k+= 2 
    Lprim = A * affineParams
    vAffine = np.matrix(np.zeros((2*len(pointNumber),1)))
    k=0
    for i in range(len(pointNumber)):
        vAffine[k] = xyPointPic1[pointNumber[i],0] - Lprim[k]
        vAffine[k+1] = xyPointPic1[pointNumber[i],1] - Lprim[k+1]
        k+=2
    RMSE_Affine = norm(vAffine)/(len(pointNumber)-n)
    return RMSE_Affine ,vAffine,Lprim

# ----------------------------------------------- Projective ------------------------------------
def V_Projective(xyPointPic1,xyPointPic2,pointNumber,projectiveParams):
    if len(pointNumber )== 1:
        n = 0
    else:
        n = 1
    A = np.matrix(np.zeros((2*len(pointNumber),8)))
    k=0
    for i in range(len(pointNumber)):
        A[k,:]=[xyPointPic2[pointNumber[i],0],xyPointPic2[pointNumber[i],1],1,0,0,0,-1*xyPointPic2[pointNumber[i],0]*xyPointPic1[pointNumber[i],0],-1*xyPointPic2[pointNumber[i],1]*xyPointPic1[pointNumber[i],0]]
        A[k+1,:]=[0,0,0,xyPointPic2[pointNumber[i],0],xyPointPic2[pointNumber[i],1],1,-1*xyPointPic2[pointNumber[i],0]*xyPointPic1[pointNumber[i],1],-1*xyPointPic2[pointNumber[i],1]*xyPointPic1[pointNumber[i],1]]
        k+= 2 
    Lprim = A * projectiveParams
    vProjective = np.matrix(np.zeros((2*len(pointNumber),1)))
    k=0
    for i in range(len(pointNumber)):
        vProjective[k] = xyPointPic1[pointNumber[i],0] - Lprim[k]
        vProjective[k+1] = xyPointPic1[pointNumber[i],1] - Lprim[k+1]
        k+=2
    RMSE_Projective = norm(vProjective)/(len(pointNumber)-n)
    return RMSE_Projective ,vProjective,Lprim

# ----------------------------------------------- Polynomial ------------------------------------
def V_Polynomial(xyPointPic1,xyPointPic2,pointNumber,polynomialParams):
    if len(pointNumber )== 1:
        n = 0
    else:
        n = 1
    A = np.matrix(np.zeros((2*len(pointNumber),12)))
    k=0
    for i in range(len(pointNumber)):
        A[k,:]=[1,xyPointPic2[pointNumber[i],0],xyPointPic2[pointNumber[i],1],xyPointPic2[pointNumber[i],0]**2,xyPointPic2[pointNumber[i],1]**2,xyPointPic2[pointNumber[i],0]*xyPointPic2[pointNumber[i],1],0,0,0,0,0,0]
        A[k+1,:]=[0,0,0,0,0,0,1,xyPointPic2[pointNumber[i],0],xyPointPic2[pointNumber[i],1],xyPointPic2[pointNumber[i],0]**2,xyPointPic2[pointNumber[i],1]**2,xyPointPic2[pointNumber[i],0]*xyPointPic2[pointNumber[i],1]]
        k+= 2 
    Lprim = A * polynomialParams
    vPolynomial = np.matrix(np.zeros((2*len(pointNumber),1)))
    k=0
    for i in range(len(pointNumber)):
        vPolynomial[k] = xyPointPic1[pointNumber[i],0] - Lprim[k]
        vPolynomial[k+1] = xyPointPic1[pointNumber[i],1] - Lprim[k+1]
        k+=2
    RMSE_Polynomial = norm(vPolynomial)/(len(pointNumber)-n)
    return RMSE_Polynomial,vPolynomial,Lprim