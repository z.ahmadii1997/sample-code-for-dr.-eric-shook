# -*- coding: utf-8 -*-
"""
Created on Sun Oct 27 10:00:49 2019

@author: Zahra Ahmadi

In this file, there are 4 functions for converting Conformal, Efine, Projective, and Polynomial. The matrices L, A are obtained according to the photographic and device coordinates and the number of control points.
"""

import numpy as np
from numpy.linalg import inv

# ----------------------------------------------- Conformal ------------------------------------
def Conformal(XY,xyPoint,controlPointNumber):
    A = np.matrix(np.zeros((2*len(controlPointNumber),4)))
    L = np.matrix(np.zeros((2*len(controlPointNumber),1)))
    k=0
    for i in range(len(controlPointNumber)) :
        L[k,0]=(XY[controlPointNumber[i],0])
        L[k+1,0]=(XY[controlPointNumber[i],1])
        A[k,:] = [xyPoint[controlPointNumber[i],0],-1*xyPoint[controlPointNumber[i],1],1,0]
        A[k+1,:]=[xyPoint[controlPointNumber[i],1],xyPoint[controlPointNumber[i],0],0,1]
        k+=2
    AT = A.transpose()
    conformalParams = (inv(AT*A)*AT*L)
    return conformalParams 

# ----------------------------------------------- Affine ------------------------------------
def Affine(XY,xyPoint,controlPointNumber):
    A = np.matrix(np.zeros((2*len(controlPointNumber),6)))
    L = np.matrix(np.zeros((2*len(controlPointNumber),1)))
    k=0
    for i in range(len(controlPointNumber)) :
        L[k,0]=(XY[controlPointNumber[i],0])
        L[k+1,0]=(XY[controlPointNumber[i],1])
        A[k,:] = [xyPoint[controlPointNumber[i],0],xyPoint[controlPointNumber[i],1],1,0,0,0]
        A[k+1,:]=[0,0,0,xyPoint[controlPointNumber[i],0],xyPoint[controlPointNumber[i],1],1]
        k+=2
    AT = A.transpose()
    affineParams = (inv(AT*A)*AT*L)
    return affineParams

# ----------------------------------------------- Projective ------------------------------------
def Projective(XY,xyPoint,controlPointNumber):
    A = np.matrix(np.zeros((2*len(controlPointNumber),8)))
    L = np.matrix(np.zeros((2*len(controlPointNumber),1)))
    k=0
    for i in range(len(controlPointNumber)) :
        L[k,0]=(XY[controlPointNumber[i],0])
        L[k+1,0]=(XY[controlPointNumber[i],1])
        A[k,:] = [xyPoint[controlPointNumber[i],0],xyPoint[controlPointNumber[i],1],1,0,0,0,-1*xyPoint[controlPointNumber[i],0]*L[k,0],-1*xyPoint[controlPointNumber[i],1]*L[k,0]]
        A[k+1,:]=[0,0,0,xyPoint[controlPointNumber[i],0],xyPoint[controlPointNumber[i],1],1,-1*xyPoint[controlPointNumber[i],0]*L[k+1,0],-1*xyPoint[controlPointNumber[i],1]*L[k+1,0]]
        k+=2
    AT = A.transpose()
    projectiveParams = (inv(AT*A)*AT*L)
    return projectiveParams

# ----------------------------------------------- Polynomial ------------------------------------
def Polynomial(XY,xyPoint,controlPointNumber):
    
    A = np.matrix(np.zeros((2*len(controlPointNumber),12)))
    L = np.matrix(np.zeros((2*len(controlPointNumber),1)))
    k=0
    for i in range(len(controlPointNumber)):
        L[k,0]= XY[controlPointNumber[i],0]
        L[k+1,0] = XY[controlPointNumber[i],1]
        A[k,:]=[1,xyPoint[controlPointNumber[i],0],xyPoint[controlPointNumber[i],1],xyPoint[controlPointNumber[i],0]**2,xyPoint[controlPointNumber[i],1]**2,xyPoint[controlPointNumber[i],0]*xyPoint[controlPointNumber[i],1],0,0,0,0,0,0]
        A[k+1,:]=[0,0,0,0,0,0,1,xyPoint[controlPointNumber[i],0],xyPoint[controlPointNumber[i],1],xyPoint[controlPointNumber[i],0]**2,xyPoint[controlPointNumber[i],1]**2,xyPoint[controlPointNumber[i],0]*xyPoint[controlPointNumber[i],1]]
        k+= 2 
    AT = A.transpose()
    polynomialParams = (inv(AT*A)*AT*L) 
    return polynomialParams