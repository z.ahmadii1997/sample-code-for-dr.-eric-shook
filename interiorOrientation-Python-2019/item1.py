# -*- coding: utf-8 -*-
"""
Spyder Editor
Created on Sat Oct 26 22:01:51 2019
@author: Zahra Ahmadi

This is the main script file.

This is the main file in which the program runs, the image coordinates are loaded, the conversion type is specified, and the device coordinates are entered. Finally, all other functions are invoked.

How to use:
Choose one model : 
1-Conformal
2-Affine
3-Projective
4-polynomial
2
1-From Picture 
2-Saved Data2
1-Left 
2-Righti1
Enter Check Point Numebr (type 'end' to exit): 1
Enter Check Point Numebr (type 'end' to exit): 4
Enter Check Point Numebr_(type 'end' to exit): end

"""

import numpy as np
import matplotlib.pyplot as plt
from convert import *
from RMSE import *
from PlotV import *
from Save import *

# fiducial xy
XY = np.loadtxt("Data.txt")
XY = np.matrix(XY)
XY = XY[:,1:]
# method
tModel = input("Choose one model : \n" +"1-Conformal \n" + "2-Affine \n" + "3-Projective \n"+"4-polynomial \n")

#Import Data
getxyPointQ = input("1-From Picture \n2-Saved Data")
getPicName = input("1-Left \n2-Right")
if getPicName == "1":
    fileName = "xyPointLeft.txt"
    folder = "Left"
    picNameTrue = "21_60 .tif"
else:
    fileName = "xyPointRight.txt"
    folder = "Right"
    picNameTrue = "22_60 .tif"
    
if getxyPointQ =="1":
    
    # Find fiducial x,y
    picture = plt.imread(picNameTrue) 
    plt.imshow(picture,cmap="gray")
    exportPoint = open(fileName,"w") 
    xyPoint=plt.ginput(0,0)
    xyPoint = np.matrix(xyPoint)
    for i in range(len(xyPoint)):
        exportPoint.write(str(i+1)+" "+str((xyPoint[i,0])) +" "+ str((xyPoint[i,1]))+'\n')
    exportPoint.close()
    xyPoint = np.matrix(xyPoint)

else:    
    try:
        xyPoint = np.loadtxt(fileName)
        xyPoint = np.matrix(xyPoint)
        xyPoint = xyPoint[:,1:]
    except IOError as e: 
         print('could not read file :',e)

# Select Check_Points and  ...
if tModel == "1":
    tresh = 5
elif tModel == "2":
    tresh = 4
elif tModel =="3":
    tresh = 3
else:
    tresh = 1
checkPointNumber = []
k=0
while  True :
    checkPointNumber.append(input("Enter Check Point Numebr (type 'end' to exit): "))
    if len(checkPointNumber) == tresh or checkPointNumber[-1].lower()=="end" or checkPointNumber[-1].isnumeric() == False:
        if len(checkPointNumber) == tresh and checkPointNumber[-1].isnumeric() == True:
            checkPointNumber[-1] = (int(checkPointNumber[-1]) -1)
            break
        else:    
            del checkPointNumber[-1] # del "end"    
            break
    else:
        checkPointNumber[-1] = (int(checkPointNumber[-1]) -1)

controlPointNumber = list(range(8))
for i in checkPointNumber :
    controlPointNumber.remove(int(i))
      
    
# -------------------------------------------- ! ------------------------------
if tModel =="1":
    exportParams = open(folder+"\\Conformal\\Conformal_"+folder+".txt","w")
    conformalParams = Conformal(XY,xyPoint,controlPointNumber)
    RMSE_Conformal_Check,vConformalCheck,Lprim_Conformal_Check = V_Conformal(XY,xyPoint,checkPointNumber,conformalParams)
    RMSE_Conformal_Control,vConformalControl,Lprim_Conformal_Control = V_Conformal(XY,xyPoint,controlPointNumber,conformalParams)
    Save(tModel,exportParams,conformalParams,vConformalCheck,vConformalControl,checkPointNumber,controlPointNumber,RMSE_Conformal_Check,RMSE_Conformal_Control)
    exportParams.close()
    PlotV(folder,"Conformal",XY,Lprim_Conformal_Control,Lprim_Conformal_Check ,controlPointNumber,checkPointNumber)
elif tModel =="2":
    exportParams = open(folder+"\\Affine\\Affine_"+folder+".txt","w")
    affineParams = Affine(XY,xyPoint,controlPointNumber)
    RMSE_Affine_Check,vAffineCheck,Lprim_Affine_Check = V_Affine(XY,xyPoint,checkPointNumber,affineParams)
    RMSE_Affine_Control,vAffineControl,Lprim_Affine_Control = V_Affine(XY,xyPoint,controlPointNumber,affineParams)
    Save(tModel,exportParams,affineParams,vAffineCheck,vAffineControl,checkPointNumber,controlPointNumber,RMSE_Affine_Check,RMSE_Affine_Control)
    exportParams.close()
    PlotV(folder,"Affine",XY,Lprim_Affine_Control ,Lprim_Affine_Check ,controlPointNumber,checkPointNumber)
elif tModel =="3":
    exportParams = open(folder+"\\Projective\\Projective_"+folder+".txt","w")
    projectiveParams = Projective(XY,xyPoint,controlPointNumber)
    RMSE_Projective_Check,vProjectiveCheck,Lprim_Projective_Check = V_Projective(XY,xyPoint,checkPointNumber,projectiveParams)
    RMSE_Projective_Control,vProjectiveControl,Lprim_Projective_Control = V_Projective(XY,xyPoint,controlPointNumber,projectiveParams)
    Save(tModel,exportParams,projectiveParams,vProjectiveCheck,vProjectiveControl,checkPointNumber,controlPointNumber,RMSE_Projective_Check,RMSE_Projective_Control)
    exportParams.close()    
    PlotV(folder,"Projective",XY,Lprim_Projective_Control ,Lprim_Projective_Check ,controlPointNumber,checkPointNumber)
elif tModel =="4":
    exportParams = open(folder+"\\Polynomial\\Polynomial_"+folder+".txt","w")
    polynomialParams = Polynomial(XY,xyPoint,controlPointNumber)
    RMSE_Polynomial_Check,vPolynomialCheck,Lprim_Polynomial_Check = V_Polynomial(XY,xyPoint,checkPointNumber,polynomialParams)
    RMSE_Polynomial_Control,vPolynomialControl,Lprim_Polynomial_Control = V_Polynomial(XY,xyPoint,controlPointNumber,polynomialParams)
    Save(tModel,exportParams,polynomialParams,vPolynomialCheck,vPolynomialControl,checkPointNumber,controlPointNumber,RMSE_Polynomial_Check,RMSE_Polynomial_Control)
    exportParams.close()
    PlotV(folder,"Polynomial",XY,Lprim_Polynomial_Control ,Lprim_Polynomial_Check,controlPointNumber,checkPointNumber)