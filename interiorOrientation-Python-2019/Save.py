# -*- coding: utf-8 -*-
"""
Created on Mon Oct 28 10:04:19 2019

@author: Zahra Ahmadi

This will be invoked by item1.

In this file, there is a function to save the conversion type, conversion parameters, the remaining vector of check and control points as well as RMSE check and checkpoints in the text file.

"""

def Save(tModel,exportParams,Params,vCheck,vControl,checkPointNumber,controlPointNumber,RMSE_Check,RMSE_Control):
    if tModel == "1":
        modelName = "Conformmal"
    elif tModel == "2":
        modelName = "Affine"
    elif tModel == "3":
        modelName = "Projective"
    else:
        modelName = "Polynomial"
    exportParams.write("----------------------------  "+ modelName +" -----------------------------"+'\n\n\n')
    exportParams.write("----------------------------  "+ modelName +" Params ----------------------"+'\n')
    for i in range(len(Params)):
        exportParams.write(str(Params[i])+'\n')
    exportParams.write(" ---------------------------- V Check ----------------------- "+'\n')
    k=0
    for i in range(len(checkPointNumber)):
        exportParams.write("Vx"+str(checkPointNumber[i]+1)+" = "+str(vCheck[k])+'\n')
        exportParams.write("Vy"+str(checkPointNumber[i]+1)+" = "+str(vCheck[k+1])+'\n')
        k+=2
    exportParams.write(" ---------------------------- RMSE Check ---------------------------- "+'\n')
    exportParams.write("RMSE Check Point = "+str(RMSE_Check)+'\n\n')
    exportParams.write(" ---------------------------- V Control ----------------------------- "+'\n')
    k=0
    for i in range(len(controlPointNumber)):
        exportParams.write("Vx"+str(controlPointNumber[i]+1)+" = "+str(vControl[k])+'\n')
        exportParams.write("Vy"+str(controlPointNumber[i]+1)+" = "+str(vControl[k+1])+'\n')
        k+=2
    exportParams.write(" ---------------------------- RMSE Control ----------------------------"+'\n')
    exportParams.write("RMSE Control Point = "+str(RMSE_Control)+'\n')