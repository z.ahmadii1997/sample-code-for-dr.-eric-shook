# -*- coding: utf-8 -*-
"""
Created on Sun Oct 27 22:01:51 2019

@author: Zahra Ahmadi

This will be invoked by item1.

 This file contains a function to display the conversion type, device coordinates, Lprim control and check, and the number of control and checkpoints.
"""

import matplotlib.pyplot as plt

def PlotV(folder,tModel,xyPointPic1,Lprimctrl,Lprimchck,ctrlNumber,chckNumber):
    plt.xlabel('X')
    plt.ylabel('Y')
    if tModel == "Conformal":
        Scale = 0.5
    else:
        Scale = 1000
    plt.title('Scale = '+str(Scale)+'  |  * = New Point',fontsize=20)
    for i in range(len(ctrlNumber)):
        x1 = xyPointPic1[ctrlNumber[i],0]
        x2 = Lprimctrl[2*i] 
        y1 = xyPointPic1[ctrlNumber[i],1]
        y2 = Lprimctrl[2*i+1]
#        print(x1,x2,y1,y2,sep = "\n\n")
        Dx = x2 - x1
        Dy = y2 - y1
        x2 = x1 + Scale*Dx
        y2 = y1 + Scale*Dy
        plt.plot([x1,x2],[y1,y2])
        plt.text(x2, y2, "ctrl"+str(ctrlNumber[i] + 1)+"*" , fontsize=10)
        plt.show()
#        
    for i in range(len(chckNumber)):
        x1 = xyPointPic1[chckNumber[i],0]
        x2 = Lprimchck[2*i] 
        y1 = xyPointPic1[chckNumber[i],1]
        y2 = Lprimchck[2*i+1] 
        Dx = x2 - x1
        Dy = y2 - y1
        x2 = x1 + Scale*Dx
        y2 = y1 + Scale*Dy
        plt.plot([x1,x2],[y1,y2])
        plt.text(x2, y2, "chck"+str(chckNumber[i] + 1)+"*" , fontsize=10)
        plt.show()
    plt.savefig(folder+"\\"+tModel+"\\"+tModel+'_'+folder+'.png')