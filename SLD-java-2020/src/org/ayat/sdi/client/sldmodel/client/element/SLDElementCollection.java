package org.ayat.sdi.client.sldmodel.client.element;

import java.util.ArrayList;

import org.ayat.sdi.client.sldmodel.client.util.UnknownElementTypeException;
import org.w3c.dom.Element;


public class SLDElementCollection<NodeWrapperType extends SLDElement> {
	ArrayList<? extends SLDElement> nodeList;
	Class<? extends SLDElement> nodeWrapperClass;

	public SLDElementCollection(ArrayList<? extends SLDElement> nodeList,
			Class<? extends SLDElement> nodeWrapperClass) {
		this.nodeList = nodeList;
		this.nodeWrapperClass = nodeWrapperClass;
	}

	@SuppressWarnings("unchecked")
	public NodeWrapperType getItem(int index)
			throws UnknownElementTypeException {
		return (NodeWrapperType) SLDElementFactory.getInstance(
				nodeWrapperClass, (Element) nodeList.get(index));
	}

	public final int getLength() {
		return nodeList.size();
	}
}