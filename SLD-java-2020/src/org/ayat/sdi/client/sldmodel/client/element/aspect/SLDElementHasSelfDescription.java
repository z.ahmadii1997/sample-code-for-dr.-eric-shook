package org.ayat.sdi.client.sldmodel.client.element.aspect;

import org.ayat.sdi.client.sldmodel.client.element.SLDElement;
import org.ayat.sdi.client.sldmodel.client.util.ElementNotFoundException;

public interface SLDElementHasSelfDescription extends SLDElement{
	public String getDescription() throws ElementNotFoundException;
	public String getName() throws ElementNotFoundException;
	public void setDescription(String description);
	public void setName(String name);
}
