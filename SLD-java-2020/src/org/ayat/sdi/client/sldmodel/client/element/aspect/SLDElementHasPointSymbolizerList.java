package org.ayat.sdi.client.sldmodel.client.element.aspect;

import org.ayat.sdi.client.sldmodel.client.element.SLDElement;
import org.ayat.sdi.client.sldmodel.client.element.SLDElementCollection;
import org.ayat.sdi.client.sldmodel.client.element.type.PointSymbolizer;

import org.w3c.dom.*;

public interface SLDElementHasPointSymbolizerList extends SLDElement {
	/**
	 * Adds a {@link PointSymbolizer}.
	 * */
	void addPointSymbolizer(PointSymbolizer value);

	/**
	 * Returns the {@link PointSymbolizer} at the specified index.
	 * */
	PointSymbolizer getPointSymbolizer(int index);

	/**
	 * Removes the specified {@link PointSymbolizer}.
	 * */
	void removePointSymbolizer(PointSymbolizer value);

	/**
	 * Returns all {@link PointSymbolizer}s.
	 * */
	SLDElementCollection<PointSymbolizer> getPointSymbolizers();

	/**
	 * Removes all {@link PointSymbolizer}s.
	 * */
	void removePointSymbolizers();

	/**
	 * Creates a {@link PointSymbolizer} using the constructor
	 * {@link PointSymbolizer}({@link Document}) and passes it to
	 * {@link #addPointSymbolizer(PointSymbolizer)}.
	 * */
	PointSymbolizer createPointSymbolizer();
}