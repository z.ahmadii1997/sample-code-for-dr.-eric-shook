package org.ayat.sdi.client.sldmodel.client.element.type.impl;

import org.ayat.sdi.client.sldmodel.client.StringPool;
import org.ayat.sdi.client.sldmodel.client.element.aspect.impl.SLDElementHasSymbolizersImpl;
import org.ayat.sdi.client.sldmodel.client.element.type.Rule;
import org.ayat.sdi.client.sldmodel.client.element.type.Symbolizer;
import org.ayat.sdi.client.sldmodel.client.util.ElementNotFoundException;

import org.w3c.dom.*;

import java.util.ArrayList;

public class RuleImpl extends SLDElementHasSymbolizersImpl implements Rule {
	public RuleImpl(Element element) {
		super(element);
	}

	public RuleImpl(Document document) {
		super(document, Rule.TAG_NAME);
	}

	@Override
	public String getName() throws ElementNotFoundException {
		return getTextGrandChildValue(StringPool.SLD_ELEM_ANY_ELEM_NAME_NAME);
	}

	@Override
	public void setName(String value) {
		setTextGrandChildValue(StringPool.SLD_ELEM_ANY_ELEM_NAME_NAME, value);
	}

	@Override
	public String getTitle() throws ElementNotFoundException {
		return getTextGrandChildValue(StringPool.SLD_ELEM_ANY_ELEM_TITLE_NAME);
	}

	@Override
	public void setTitle(String value) {
		setTextGrandChildValue(StringPool.SLD_ELEM_ANY_ELEM_TITLE_NAME, value);
	}

	@Override
	public String getAbstract() throws ElementNotFoundException {
		return getTextGrandChildValue(StringPool.SLD_ELEM_ANY_ELEM_ABSTRACT_NAME);
	}

	@Override
	public void setAbstract(String value) {
		setTextGrandChildValue(StringPool.SLD_ELEM_ANY_ELEM_ABSTRACT_NAME,
				value);
	}

	@Override
	public Double getMinScaleDenominator() throws NumberFormatException,
			ElementNotFoundException {
		return getDoubleGrandChildValue(StringPool.SLD_ELEM_RULE_ELEM_MINSCALEDENOM_NAME);
	}

	@Override
	public void setMinScaleDenominator(Double value) {
		setDoubleGrandChildValue(
				StringPool.SLD_ELEM_RULE_ELEM_MINSCALEDENOM_NAME, value);
	}

	@Override
	public Double getMaxScaleDenominator() throws NumberFormatException,
			ElementNotFoundException {
		return getDoubleGrandChildValue(StringPool.SLD_ELEM_RULE_ELEM_MAXSCALEDENOM_NAME);
	}

	@Override
	public void setMaxScaleDenominator(Double value) {
		setDoubleGrandChildValue(
				StringPool.SLD_ELEM_RULE_ELEM_MAXSCALEDENOM_NAME, value);
	}

	@Override
	public ArrayList<Symbolizer> getSymbolizers() {
		return null;
	}
}
