package org.ayat.sdi.client.sldmodel.client.element.aspect;

import org.ayat.sdi.client.sldmodel.client.element.SLDElement;
import org.ayat.sdi.client.sldmodel.client.element.type.Graphic;
import org.ayat.sdi.client.sldmodel.client.util.ElementNotFoundException;

import org.w3c.dom.*;

public interface SLDElementHasGraphic extends SLDElement {
	/**
	 * @throws ElementNotFoundException
	 */
	Graphic getGraphic() throws ElementNotFoundException;

	void setGraphic(Graphic graphic);

	/**
	 * Creates a {@link Graphic} using the constructor {@link Graphic}(
	 * {@link Document}) and passes it to {@link #setGraphic(Graphic)}.
	 * */
	Graphic createGraphic();

	/**
	 * If a {@link Graphic} has been set before, removes it. Else does nothing.
	 * */
	void removeGraphic();
}
