package org.ayat.sdi.client.sldmodel.client.element.type.impl;

import java.util.ArrayList;

import org.ayat.sdi.client.sldmodel.client.StringPool;
import org.ayat.sdi.client.sldmodel.client.element.AbstractSLDElement;
import org.ayat.sdi.client.sldmodel.client.element.type.DashArray;
import org.ayat.sdi.client.sldmodel.client.util.InvalidTextFatherException;

import org.w3c.dom.*;

public class DashArrayImpl extends AbstractSLDElement implements DashArray {
	ArrayList<Double> values = new ArrayList<Double>();

	public DashArrayImpl(Document document) {
		super(document, StringPool.SLD_ELEM_STROKE_ELEM_DASHARRAY_NAME);
	}

	public DashArrayImpl(Element element) {
		super(element);
//		parseValues();
	}

	protected void parseValues() {
		try {
			values.clear();
			String[] doubleStrings = getTextValue().split(StringPool.SPACE);
			for(String doubleString:doubleStrings)
				values.add(Double.parseDouble(doubleString));
		} catch (InvalidTextFatherException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public void add(Double value) {
		values.add(value);
		updateElement();
	}

	@Override
	public Double get(int index) {
		return values.get(index);
	}

	@Override
	public ArrayList<Double> get() {
		return values;
	}

	@Override
	public void remove(int index) {
		values.remove(index);
		updateElement();
	}

	protected void updateElement() {
		StringBuffer result = new StringBuffer();
		for (Double d : values)
			result.append(d + StringPool.SPACE);
		if (result.length() > 0)
			result.deleteCharAt(result.lastIndexOf(StringPool.SPACE));
		setTextValue(result.toString());
	}

	@Override
	public void clear() {
		setTextValue("");
		values.clear();
	}
}
