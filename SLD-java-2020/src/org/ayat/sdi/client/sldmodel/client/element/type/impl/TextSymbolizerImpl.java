package org.ayat.sdi.client.sldmodel.client.element.type.impl;

import org.ayat.sdi.client.sldmodel.client.StringPool;
import org.ayat.sdi.client.sldmodel.client.element.SLDElementFactory;
import org.ayat.sdi.client.sldmodel.client.element.aspect.impl.SLDElementHasFillImpl;
import org.ayat.sdi.client.sldmodel.client.element.type.Font;
import org.ayat.sdi.client.sldmodel.client.element.type.Halo;
import org.ayat.sdi.client.sldmodel.client.element.type.TextSymbolizer;
import org.ayat.sdi.client.sldmodel.client.util.ElementNotFoundException;
import org.ayat.sdi.client.sldmodel.client.util.UnknownElementTypeException;

import org.w3c.dom.*;

public class TextSymbolizerImpl extends SLDElementHasFillImpl implements
		TextSymbolizer {
	public TextSymbolizerImpl(Document document) {
		super(document, TextSymbolizer.TAG_NAME);
	}

	public TextSymbolizerImpl(Element element) {
		super(element);
	}

	@Override
	public String getLabel() throws ElementNotFoundException {
		return getTextGrandChildValue(StringPool.SLD_ELEM_TEXTSYMBOLIZER_ELEM_LABEL_NAME);
	}

	@Override
	public void setLabel(String value) {
		setTextGrandChildValue(
				StringPool.SLD_ELEM_TEXTSYMBOLIZER_ELEM_LABEL_NAME, value);
	}

	@Override
	public void setFont(Font value) {
		setUniqueChild(value.getElement());
	}

	@Override
	public Font getFont() throws ElementNotFoundException {
		try {
			return SLDElementFactory.getInstance(Font.class,
					getUniqueChild(Font.TAG_NAME));
		} catch (UnknownElementTypeException e) {// Will never happen
			throw new RuntimeException(e);
		}
	}

	@Override
	public Font createFont() {
		try {
			Font result = SLDElementFactory.getInstance(Font.class, document);
			setFont(result);
			return result;
		} catch (UnknownElementTypeException e) {// Will never happen
			throw new RuntimeException(e);
		}
	}

	@Override
	public void removeFont() {
		try {
			element.removeChild(getFont().getElement());
		} catch (ElementNotFoundException e) {
			// Nothing to remove.
		}
	}

	@Override
	public void setLabelField(String fieldName) {
		Element ogcPropertyName = document
				.createElement(StringPool.OGC_ELEM_ANY_ELEM_PROPERTYNAME_NAME);
		ogcPropertyName.appendChild(document.createTextNode(fieldName));
		Element label = document
				.createElement(StringPool.SLD_ELEM_TEXTSYMBOLIZER_ELEM_LABEL_NAME);
		label.appendChild(ogcPropertyName);
		setUniqueChild(label);
	}

	@Override
	public void setHalo(Halo value) {
		setUniqueChild(value.getElement());
	}

	@Override
	public Halo getHalo() throws ElementNotFoundException {
		try {
			return SLDElementFactory.getInstance(Halo.class,
					getUniqueChild(Halo.TAG_NAME));
		} catch (Exception e) {
			throw new ElementNotFoundException(Halo.TAG_NAME, e);
		}
	}

	@Override
	public Halo createHalo() {
		try {
			Halo result = SLDElementFactory.getInstance(Halo.class, document);
			setHalo(result);
			return result;
		} catch (UnknownElementTypeException e) {// Will never happen
			throw new RuntimeException(e);
		}
	}

	@Override
	public void removeHalo() {
		try {
			element.removeChild(getHalo().getElement());
		} catch (ElementNotFoundException e) {
			// Do nothing. there is no halo to be removed.
		}
	}
}
