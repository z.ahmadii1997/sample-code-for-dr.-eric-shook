package org.ayat.sdi.client.sldmodel.client.core;

public class StringPool {
	public static int debugLevel = 10;
	public static final String QUESTION_MARK = "?";
	public static final String EQUAL_SIGN = "=";
	public static final String COMMA = ",";
	public static final String COLON = ":";
	public static final String DASH = "-";
	public static final String DOT = ".";
	public static final String DOT_DOT_DOT = "...";
	public static final String AMPERSAND = "&";
	public static final String SPACE = " ";
	public static final String NEW_LINE = "\n";
	public static final String TRUE = "TRUE";
	public static final String FALSE = "FALSE";
	public static final String SHARP = "#";
	public static final String DOLLAR_SIGN = "$";
	public static final String BRACE_OPEN = "{";
	public static final String BRACE_CLOSE = "}";
	public static final String BRAKET_OPEN = "[";
	public static final String BRAKET_CLOSE = "]";
	public static final String SINGLE_QUOTE = "'";
	public final static String PIPE_LINE = "|";
	public static final String PERCENT = "%";
	public static final int HUNDRED = 100;
	public static final String HUNDRED_PERCENT = HUNDRED + PERCENT;
	public final static String EMPTY = "";
	public final static String FORWARDSLASH = "/";
	public final static String BACKWARDSLASH = "\\";
	public final static String HTTP = "http";
	public final static String HTTP_URL_PREFIX = HTTP + COLON + FORWARDSLASH
			+ FORWARDSLASH;

	public static final String RES_URL_IMG_SEPARATOR = "theme/images/controls/separator.gif";

	public static final String GEOSERVER_ON_SKY2 = "http://10.1.2.44:8080/geoserver";
	public static final String GEOSERVER_ON_LOCALHOST = "http://localhost:8080/geoserver";
	public static final String GOOGLE_MAPS_API_KEY = "ABQIAAAAjpkAC9ePGem0lIq5XcMiuhR_wWLPFku8Ix9i2SXYRVK3e45q1BQUd_beF8dtzKET_EteAjPdGDwqpQ";

	public static enum GEOMETRY_TYPE {
		POINT, LINE, POLYGON;
		public static GEOMETRY_TYPE parse(String value) {
			GEOMETRY_TYPE result = null;
			// Check point first.
			if (value.toLowerCase().contains("point"))
				result = POINT;
			if (value.toLowerCase().contains("line"))
				result = LINE;
			if (value.toLowerCase().contains("polygon"))
				result = POLYGON;
			return result;
		}
	}

	public static enum MAP_PROJECTION {
		EPSG4326, EPSG80001, EPSG26713, EPSG900913;
		public String toString() {
			String result = new String();
			switch (this) {
			case EPSG4326:
				result = "EPSG:4326, Lattitude/Longtitude Standard";
				break;
			case EPSG80001:
				result = "EPSG:80001, Iranian SDI Standard";
				break;

			case EPSG900913:
				result = "EPSG:900913, Spherical Mercator, Community standard for commercial layers";
				break;
			case EPSG26713:
				result = "EPSG:26713";
				break;
			}
			return result;
		}

		public String toEPSGCode() {
			String result = new String();
			switch (this) {
			case EPSG4326:
				result = "EPSG:4236";
				break;
			case EPSG80001:
				result = "EPSG:80001";
				break;
			case EPSG900913:
				result = "EPSG:900913";
				break;
			case EPSG26713:
				result = "EPSG:26713";
			}
			return result;
		}

		public static MAP_PROJECTION valueOf_Colon(String value) {
			MAP_PROJECTION result = null;
			if (value.equals("EPSG:4326"))
				result = EPSG4326;
			else if (value.equals("EPSG:80001"))
				result = EPSG80001;
			else if (value.equals("EPSG:900913"))
				result = EPSG900913;
			else if (value.equals("EPSG:26713"))
				result = EPSG26713;
			return result;
		}
	};

	public static enum MIME_TYPE {
		GIF, PNG, CHART;
		public String toString() {
			String result = new String();
			switch (this) {
			case GIF:
				result = "GIF format, IE6 Compatible";
				break;
			case PNG:
				result = "PNG format, higher performance, higher quality";
				break;
			}
			return result;
		}

		public String toMIMEString() {
			String result = new String();
			switch (this) {
			case GIF:
				result = MIMETYPE_IMG_GIF;
				break;
			case PNG:
				result = MIMETYPE_IMG_PNG;
				break;
			case CHART:
				result = MIMETYPE_APP_CHART;
				break;
			}
			return result;
		}

		public static MIME_TYPE parse(String value) {
			MIME_TYPE result = null;
			if (value.equals(MIMETYPE_IMG_GIF))
				result = MIME_TYPE.GIF;
			if (value.equals(MIMETYPE_IMG_PNG))
				result = MIME_TYPE.PNG;
			if (value.equals(MIMETYPE_APP_CHART))
				result = MIME_TYPE.CHART;
			return result;
		}
	};

	public static final String MIMETYPE_IMG_GIF = "image/gif";
	public static final String MIMETYPE_APP_CHART = "application/chart";
	public static final String MIMETYPE_IMG_PNG = "image/png";
	public static final int MAP_ZOOM_LEVELS = 20;
	public static final MAP_PROJECTION DEFAULT_MAP_PROJECTION = MAP_PROJECTION.EPSG900913;
	public static MIME_TYPE WMS_DEFAULT_IMAGE_FORMAT = MIME_TYPE.PNG;

	public static final double GOOGLE_MAPS_CENTER_LONLAT_X = 10.2;
	public static final double GOOGLE_MAPS_CENTER_LONLAT_Y = 48.9;

	public static final double GOOGLE_MAPS_MIN_LONLAT_X = -180;
	public static final double GOOGLE_MAPS_MIN_LONLAT_Y = -90;

	public static final double GOOGLE_MAPS_MAX_LONLAT_X = 180;
	public static final double GOOGLE_MAPS_MAX_LONLAT_Y = 90;

	public static enum MAP_BACKGROUND {
		NONE, GM_HYBRID, GM_STREET, GM_SATELLITE;
		public String toString() {
			String result = new String();
			switch (this) {
			case NONE:
				result = "None";
				break;
			case GM_HYBRID:
				result = "GoogleMaps - Hybrid";
				break;
			case GM_SATELLITE:
				result = "GoogleMaps - Satellite";
				break;
			case GM_STREET:
				result = "GoogleMaps - Street";
				break;
			}
			return result;
		}
	};
}
