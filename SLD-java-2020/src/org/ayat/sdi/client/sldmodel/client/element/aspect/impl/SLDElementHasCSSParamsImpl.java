package org.ayat.sdi.client.sldmodel.client.element.aspect.impl;

import java.util.ArrayList;

import org.ayat.sdi.client.sldmodel.client.StringPool;
import org.ayat.sdi.client.sldmodel.client.element.AbstractSLDElement;
import org.ayat.sdi.client.sldmodel.client.element.SLDElementCollection;
import org.ayat.sdi.client.sldmodel.client.element.SLDElementFactory;
import org.ayat.sdi.client.sldmodel.client.element.SLDElementList;
import org.ayat.sdi.client.sldmodel.client.element.aspect.SLDElementHasCSSParameterList;
import org.ayat.sdi.client.sldmodel.client.element.type.CSSParameter;
import org.ayat.sdi.client.sldmodel.client.util.ElementNotFoundException;
import org.ayat.sdi.client.sldmodel.client.util.UnknownElementTypeException;

import org.w3c.dom.*;

public abstract class SLDElementHasCSSParamsImpl extends AbstractSLDElement
		implements SLDElementHasCSSParameterList {
	protected ArrayList<CSSParameter> cssParams = new ArrayList<CSSParameter>();

	public SLDElementHasCSSParamsImpl(Document document, String tagName) {
		super(document, tagName);
	}

	public SLDElementHasCSSParamsImpl(Element element) {
		super(element);
		parseCSSParams();
	}

	protected void parseCSSParams() {
		try {
			cssParams.clear();
			SLDElementList<CSSParameter> cssParamNodeList = new SLDElementList<CSSParameter>(
					element.getElementsByTagName(CSSParameter.TAG_NAME),
					CSSParameter.class);
			for (int i = 0; i < cssParamNodeList.getLength(); i++)
				cssParams.add(cssParamNodeList.getItem(i));
		} catch (UnknownElementTypeException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public void addCSSParameter(CSSParameter value) {
		cssParams.add(value);
		element.appendChild(value.getElement());
	}

	@Override
	public void removeCSSParameter(CSSParameter value) {
		element.removeChild(value.getElement());
		cssParams.remove(value);
	}

	@Override
	public SLDElementCollection<CSSParameter> getCSSParameters() {
		return new SLDElementCollection<CSSParameter>(cssParams,
				CSSParameter.class);
	}

	@Override
	public CSSParameter getCSSParameter(int index) {
		return cssParams.get(index);
	}

	@Override
	public CSSParameter getUniqueCSSParameter(String cssRuleName)
			throws ElementNotFoundException {
		for (CSSParameter cssParam : cssParams)
			if (cssParam.getCSSRuleName().equals(cssRuleName))
				return cssParam;
		throw new ElementNotFoundException("CSSParameter containing "
				+ cssRuleName);
	}

	@Override
	public void setUniqueCSSParameter(CSSParameter value) {
		ArrayList<CSSParameter> toRemove = new ArrayList<CSSParameter>();
		for (CSSParameter cssParam : cssParams)
			if (cssParam.getCSSRuleName().equals(value.getCSSRuleName()))
				toRemove.add(cssParam);
		for (CSSParameter cssParam : toRemove)
			removeCSSParameter(cssParam);
		addCSSParameter(value);
	}

	@Override
	public CSSParameter createCSSParameter() {
		try {
			CSSParameter result = SLDElementFactory.getInstance(
					CSSParameter.class, document);
			addCSSParameter(result);
			return result;
		} catch (UnknownElementTypeException e) {// Will never happen
			throw new RuntimeException(e);
		}
	}
 
	@Override
	public void setUniqueOGCLiteral(CSSParameter value, String literal) {
		Element ogcLiteral = document
				.createElement(StringPool.OGC_ELEM_ANY_ELEM_LITERAL_NAME);
		ogcLiteral.appendChild(document.createTextNode(literal));
		value.getElement().appendChild(ogcLiteral);
		setUniqueCSSParameter(value);
	}
}
