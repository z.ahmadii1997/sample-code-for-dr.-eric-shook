package org.ayat.sdi.client.sldmodel.client.element;

import java.io.File;
import java.io.StringWriter;
import java.util.ArrayList;

import org.ayat.sdi.client.sldmodel.client.Utility;
import org.ayat.sdi.client.sldmodel.client.util.ElementNotFoundException;
import org.ayat.sdi.client.sldmodel.client.util.InvalidTextFatherException;

import org.w3c.dom.*;

import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMResult;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

public abstract class AbstractSLDElement implements SLDElement {
	protected Document document;
	protected Element element;
	protected String tagName;
	protected ArrayList<Node> textFathers;

	private AbstractSLDElement(Document document, Element element) {
		this.document = document;
		this.element = element;
		textFathers = new ArrayList<Node>();
		initElement();
	}

	protected void parseTextFathers() {
		textFathers.clear();
		NodeList children = element.getChildNodes();
		for (int i = 0; i < children.getLength(); i++)
			if (children.item(i).getNodeType() != 3)
				// if child is not a text node.
				if (children.item(i).getChildNodes().getLength() == 1)
					// and has only one child
					if (children.item(i).getChildNodes().item(0).getNodeType() == 3)
						// which is a text node
						textFathers.add(children.item(i));
	}

	public AbstractSLDElement(Document document, String tagName) {
		this(document, document.createElement(tagName));
		this.tagName = tagName;
	}

	public AbstractSLDElement(Element element) {
		this(element.getOwnerDocument(), element);
		parseTextFathers();
	}

	/**
	 * Will be called when creating the element. Can be overridden to create a
	 * mandatory child or set such attribute. Does nothing if not overridden.
	 * */
	protected void doInitElement() {
	}

	/**
	 * Safely retrieves a grand child text value that stores a boolean.
	 * 
	 * @throws ElementNotFoundException
	 */
	protected Boolean getBooleanGrandChildValue(String childElementName)
			throws ElementNotFoundException {
		return Boolean.parseBoolean(getTextGrandChildValue(childElementName));
	}

	public Document getDocument() {
		return document;
	}

	/**
	 * Safely retrieves a grand child text value that stores a double.
	 * 
	 * @throws ElementNotFoundException
	 * @throws NumberFormatException
	 */
	protected Double getDoubleGrandChildValue(String childElementName)
			throws NumberFormatException, ElementNotFoundException {
		return Double.parseDouble(getTextGrandChildValue(childElementName));
	};

	public Element getElement() {
		return element;
	}

	/**
	 * Safely retrieves a grand child text value that stores an integer.
	 * 
	 * @throws ElementNotFoundException
	 * @throws NumberFormatException
	 */
	protected Integer getIntegerGrandChildValue(String childElementName)
			throws NumberFormatException, ElementNotFoundException {
		return Integer.parseInt(getTextGrandChildValue(childElementName));
	}

	public String getTagName() {
		return tagName;
	}

	/**
	 * Returns value set before using setGrandChildTextValue() for this
	 * childElementName.
	 * 
	 * @throws ElementNotFoundException
	 * */
	protected String getTextGrandChildValue(String childElementName)
			throws ElementNotFoundException {
		Text textNode = null;

		for (Node father : textFathers)
			if (father.getNodeName().equals(childElementName)) {
				// we do have such node
				textNode = (Text) father.getFirstChild();
				break;
			}
		if (textNode == null) // we don't have such node
			throw new ElementNotFoundException(childElementName);
		return textNode.getData();
	}

	/**
	 * Returns the text wrapped by the element we're wrapping.<br/>
	 * The use of this method is essentially meaningless if the value expected
	 * hasn't been set using setTextValue().
	 * 
	 * @throws InvalidTextFatherException
	 * */
	protected String getTextValue() throws InvalidTextFatherException {
		try {
			Text text = (Text) element.getChildNodes().item(0);
			return text.getData();
		} catch (Exception e) {
			throw new InvalidTextFatherException(e);
		}
	}

	/**
	 * Convenience method.<br/>
	 * Note: The use of this method is essentially meaningless unless the value
	 * for the child in hand has been set using setUniqueChild().
	 * 
	 * @throws ElementNotFoundException
	 */
	protected Element getUniqueChild(String childName)
			throws ElementNotFoundException {
		Node result = null;
		NodeList nodes = element.getElementsByTagName(childName);
		if (nodes.getLength() > 0)
			result = nodes.item(0);
		if (result == null)
			throw new ElementNotFoundException(childName);
		return (Element) result;
	}

	protected void initElement() {
		doInitElement();
	}

	/**
	 * Convenience method.
	 */
	protected void setBooleanGrandChildValue(String childElementName,
			boolean value) {
		setTextGrandChildValue(childElementName, Boolean.toString(value));
	}

	/**
	 * Convenience method.
	 */
	protected void setDoubleGrandChildValue(String childElementName,
			double value) {
		setTextGrandChildValue(childElementName, Double.toString(value));
	}

	/**
	 * Convenience method.
	 */
	protected void setIntegerGrandChildValue(String childElementName, int value) {
		setTextGrandChildValue(childElementName, Integer.toString(value));
	}

	/**
	 * If this is the first time value is being set for this childElementName,
	 * creates a child element by the name supplied as childElementName and
	 * appends as its' first child a text node that shall contain
	 * grandChildTextValue. If not, It is implied that a node by this
	 * childElementName has been created before, and that it contains one and
	 * only one child which is a TextNode. So value for that TextNode is set to
	 * grandChildTextValue.
	 * */
	protected void setTextGrandChildValue(String childElementName,
			String grandChildTextValue) {
		Text textNode = null;

		for (Node father : textFathers)
			if (father.getNodeName().equals(childElementName)) {
				// we already have such node
				textNode = (Text) father.getFirstChild();
				break;
			}
		if (textNode == null) {// we haven't had such node before
			Element father = document.createElement(childElementName);
			textNode = document.createTextNode("");
			father.appendChild(textNode);// shall be the first and only child
			element.appendChild(father);
			textFathers.add(father);
		}
		textNode.setData(grandChildTextValue);
	}

	/**
	 * Drops all child elements and then adds a TextNode to the element we're
	 * wrapping and sets its' value to the string passed to it.
	 * */
	protected void setTextValue(String value) {
		Utility.removeAllChildren(element);
		element.appendChild(document.createTextNode(value));
	}

	/**
	 * Using this method to append an element, You make sure there is only one
	 * instance of it among your children at any given time.
	 */
	protected void setUniqueChild(Element uniqueChild) {
		NodeList children = element.getChildNodes();
		for (int i = 0; i < children.getLength(); i++)
			if (children.item(i).getNodeName()
					.equals(uniqueChild.getNodeName())) {
				element.replaceChild(uniqueChild, children.item(i));
				return;
			}
		// if we made it this far, it means we don't have a node with the same
		// name as our new node.
		element.appendChild(uniqueChild);
	}

	@Override
	public String toString() {
		TransformerFactory tFactory = TransformerFactory.newInstance();
		try {
			Transformer transformer = tFactory.newTransformer();
			DOMSource source = new DOMSource(document);
			StringWriter writer = new StringWriter();
			StreamResult result = new StreamResult(writer);

			transformer.transform(source,result );
			return writer.getBuffer().toString();
		} catch (TransformerConfigurationException e) {
			e.printStackTrace();
		} catch (TransformerException e) {
			e.printStackTrace();
		}
		return element.toString();
	}
}
