package org.ayat.sdi.client.sldmodel.client.element.type;

import org.ayat.sdi.client.sldmodel.client.StringPool;
import org.ayat.sdi.client.sldmodel.client.element.aspect.SLDElementHasFill;
import org.ayat.sdi.client.sldmodel.client.element.aspect.SLDElementHasFont;
import org.ayat.sdi.client.sldmodel.client.element.aspect.SLDElementHasHalo;
import org.ayat.sdi.client.sldmodel.client.util.ElementNotFoundException;

public interface TextSymbolizer extends Symbolizer, SLDElementHasFill,
		SLDElementHasFont,SLDElementHasHalo {
	String TAG_NAME = StringPool.SLD_ELEM_TEXTSYMBOLIZER_NAME;

	/**
	 * @throws ElementNotFoundException
	 */
	String getLabel() throws ElementNotFoundException;

	void setLabel(String value);

	/**
	 * Sets the name of the field to be used for label.
	 * */
	void setLabelField(String fieldName);
}
