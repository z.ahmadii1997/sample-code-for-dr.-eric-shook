package org.ayat.sdi.client.sldmodel.client.element.aspect.impl;

import org.ayat.sdi.client.sldmodel.client.element.AbstractSLDElement;
import org.ayat.sdi.client.sldmodel.client.element.SLDElementFactory;
import org.ayat.sdi.client.sldmodel.client.element.aspect.SLDElementHasStroke;
import org.ayat.sdi.client.sldmodel.client.element.type.Stroke;
import org.ayat.sdi.client.sldmodel.client.util.ElementNotFoundException;
import org.ayat.sdi.client.sldmodel.client.util.UnknownElementTypeException;

import org.w3c.dom.*;

public abstract class SLDElementHasStrokeImpl extends
		AbstractSLDElement implements SLDElementHasStroke{
	public SLDElementHasStrokeImpl(Document document, String tagName) {
		super(document, tagName);
	}

	public SLDElementHasStrokeImpl(Element element) {
		super(element);
	}

	@Override
	public void setStroke(Stroke stroke) {
		setUniqueChild(stroke.getElement());
	}
	
	@Override
	public Stroke getStroke() throws ElementNotFoundException {
		try {
			return SLDElementFactory.getInstance(Stroke.class,
					getUniqueChild(Stroke.TAG_NAME));
		} catch (Exception e) {
			throw new ElementNotFoundException(Stroke.TAG_NAME,e);
		}
	}
	
	@Override
	public Stroke createStroke() {
		try {
			Stroke result= SLDElementFactory.getInstance(Stroke.class, document);
			setStroke(result);
			return result;
		} catch (UnknownElementTypeException e) {//Will never happen
			throw new RuntimeException(e);
		}
	}
	
	@Override
	public void removeStroke() {
		try {
			element.removeChild(getStroke().getElement());
		} catch (ElementNotFoundException e) {
			// Do nothing. there is no stroke to be removed.
		}
	}
}
