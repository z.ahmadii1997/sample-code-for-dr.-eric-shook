package org.ayat.sdi.client.sldmodel.client.element.type.impl;

import org.ayat.sdi.client.sldmodel.client.element.aspect.impl.SLDElementHasCSSParamsHasGraphicImpl;
import org.ayat.sdi.client.sldmodel.client.element.type.GraphicFill;

import org.w3c.dom.*;

public class GraphicFillImpl extends SLDElementHasCSSParamsHasGraphicImpl
		implements GraphicFill {
	public GraphicFillImpl(Document document) {
		super(document, GraphicFill.TAG_NAME);
	}

	public GraphicFillImpl(Element element) {
		super(element);
	}
}
