package org.ayat.sdi.client.sldmodel.client;


import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

public class Utility extends  org.ayat.sdi.client.sldmodel.client.core.Utility{
	/**Removes all of the children of a given element.
	 * */
	public static void removeAllChildren(Element element){
		NodeList nodes=element.getChildNodes();
		for(int i=nodes.getLength()-1;i>=0;i--){
			element.removeChild(nodes.item(i));
		}
	}
}
