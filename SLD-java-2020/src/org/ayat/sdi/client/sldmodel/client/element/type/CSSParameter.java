package org.ayat.sdi.client.sldmodel.client.element.type;

import org.ayat.sdi.client.sldmodel.client.StringPool;
import org.ayat.sdi.client.sldmodel.client.element.SLDElement;
import org.ayat.sdi.client.sldmodel.client.util.InvalidTextFatherException;

public interface CSSParameter extends SLDElement {
	String TAG_NAME = StringPool.SLD_ELEM_CSSPARAMETER_NAME;

	/**
	 * Returns the name of this CSS rule. e.g. if font-family:Tahoma; returns
	 * "font-family".
	 */
	String getCSSRuleName();

	/** Sets the name of this CSS rule. */
	void setCSSRuleName(String value);

	/**
	 * Returns the value of this CSS rule. e.g. if font-family:Tahoma; returns
	 * "Tahoma".
	 * 
	 * @throws InvalidTextFatherException
	 */
	String getCSSRuleValue() throws InvalidTextFatherException;

	/** Sets the value of this CSS rule. */
	void setCSSRuleValue(String value);
}
