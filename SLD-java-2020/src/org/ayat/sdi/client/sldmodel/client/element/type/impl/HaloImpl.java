package org.ayat.sdi.client.sldmodel.client.element.type.impl;

import org.ayat.sdi.client.sldmodel.client.StringPool;
import org.ayat.sdi.client.sldmodel.client.element.aspect.impl.SLDElementHasFillImpl;
import org.ayat.sdi.client.sldmodel.client.element.type.Halo;
import org.ayat.sdi.client.sldmodel.client.util.ElementNotFoundException;

import org.w3c.dom.*;

public class HaloImpl extends SLDElementHasFillImpl implements Halo {
	public HaloImpl(Document document) {
		super(document, Halo.TAG_NAME);
	}

	public HaloImpl(Element element) {
		super(element);
	}

	@Override
	public int getRadius() throws NumberFormatException,
			ElementNotFoundException {
		return getIntegerGrandChildValue(StringPool.SLD_ELEM_HALO_ELEM_RADIUS_NAME);
	}

	@Override
	public void setRadius(int value) {
		setIntegerGrandChildValue(StringPool.SLD_ELEM_HALO_ELEM_RADIUS_NAME,
				value);
	}
}
