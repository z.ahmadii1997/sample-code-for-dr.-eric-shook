package org.ayat.sdi.client.sldmodel.client.element.type;

import org.ayat.sdi.client.sldmodel.client.StringPool;
import org.ayat.sdi.client.sldmodel.client.StringPool.SLD_ELEM_ROOT_ATTR_VERSION_VAL;
import org.ayat.sdi.client.sldmodel.client.element.aspect.SLDElementHasNamedLayerList;
import org.ayat.sdi.client.sldmodel.client.element.aspect.SLDElementHasSelfDescription;

public interface StyledLayerDescriptor extends SLDElementHasSelfDescription,
		SLDElementHasNamedLayerList {
	String TAG_NAME = StringPool.SLD_ELEM_ROOT_NAME;

	/** Returns null if no value has been set before.* */
	SLD_ELEM_ROOT_ATTR_VERSION_VAL getVersion();

	void setVersion(SLD_ELEM_ROOT_ATTR_VERSION_VAL version);
}