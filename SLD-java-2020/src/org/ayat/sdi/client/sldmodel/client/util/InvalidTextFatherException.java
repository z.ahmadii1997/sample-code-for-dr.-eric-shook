package org.ayat.sdi.client.sldmodel.client.util;

public class InvalidTextFatherException extends Exception{
	private static final long serialVersionUID = 3787216435659029219L;

	public InvalidTextFatherException() {
		super();
	}
	
	public InvalidTextFatherException(String message, Throwable cause) {
		super(message, cause);
	}

	public InvalidTextFatherException(String message) {
		super(message);
	}

	public InvalidTextFatherException(Throwable cause) {
		super(cause);
	}
}
