package org.ayat.sdi.client.sldmodel.client.element.aspect;

import org.ayat.sdi.client.sldmodel.client.element.SLDElement;
import org.ayat.sdi.client.sldmodel.client.element.type.ExternalGraphic;
import org.ayat.sdi.client.sldmodel.client.util.ElementNotFoundException;

import org.w3c.dom.*;

public interface SLDElementHasExternalGraphic extends SLDElement{
	/**
	 * @throws ElementNotFoundException
	 */
	ExternalGraphic getExternalGraphic() throws ElementNotFoundException;

	void setExternalGraphic(ExternalGraphic value);

	/**
	 * If a {@link ExternalGraphic} has been set before, removes it. Else does
	 * nothing.
	 * */
	void removeExternalGraphic();

	/**
	 * Creates an {@link ExternalGraphic} using the constructor
	 * {@link ExternalGraphic}({@link Document}) and passes it to
	 * {@link #setExternalGraphic(ExternalGraphic)}.
	 * */
	ExternalGraphic createExternalGraphic();
}
