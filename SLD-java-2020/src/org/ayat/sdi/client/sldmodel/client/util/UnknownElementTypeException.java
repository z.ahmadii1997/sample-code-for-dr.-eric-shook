package org.ayat.sdi.client.sldmodel.client.util;

public class UnknownElementTypeException extends Exception{
	private static final long serialVersionUID = 3787216435659029219L;

	public UnknownElementTypeException() {
		super();
	}
	
	public UnknownElementTypeException(String message, Throwable cause) {
		super(message, cause);
	}

	public UnknownElementTypeException(String message) {
		super(message);
	}

	public UnknownElementTypeException(Throwable cause) {
		super(cause);
	}
}
