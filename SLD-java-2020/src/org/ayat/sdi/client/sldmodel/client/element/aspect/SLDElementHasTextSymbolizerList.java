package org.ayat.sdi.client.sldmodel.client.element.aspect;

import org.ayat.sdi.client.sldmodel.client.element.SLDElement;
import org.ayat.sdi.client.sldmodel.client.element.SLDElementCollection;
import org.ayat.sdi.client.sldmodel.client.element.type.TextSymbolizer;

import org.w3c.dom.*;

public interface SLDElementHasTextSymbolizerList extends SLDElement {
	/**
	 * Adds a {@link TextSymbolizer}.
	 * */
	void addTextSymbolizer(TextSymbolizer value);

	/**
	 * Returns the {@link TextSymbolizer} at the specified index.
	 * */
	TextSymbolizer getTextSymbolizer(int index);

	/**
	 * Removes the specified {@link TextSymbolizer}.
	 * */
	void removeTextSymbolizer(TextSymbolizer value);

	/**
	 * Returns all {@link TextSymbolizer}s.
	 * */
	SLDElementCollection<TextSymbolizer> getTextSymbolizers();

	/**
	 * Removes all {@link TextSymbolizer}s.
	 * */
	void removeTextSymbolizers();

	/**
	 * Creates a {@link TextSymbolizer} using the constructor
	 * {@link TextSymbolizer}({@link Document}) and passes it to
	 * {@link #addTextSymbolizer(TextSymbolizer)}.
	 * */
	TextSymbolizer createTextSymbolizer();
}