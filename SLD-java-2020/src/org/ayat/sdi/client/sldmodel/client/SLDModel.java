package org.ayat.sdi.client.sldmodel.client;

import org.ayat.sdi.client.sldmodel.client.element.type.FeatureTypeStyle;
import org.ayat.sdi.client.sldmodel.client.element.type.Fill;
import org.ayat.sdi.client.sldmodel.client.element.type.NamedLayer;
import org.ayat.sdi.client.sldmodel.client.element.type.StyledLayerDescriptor;
import org.ayat.sdi.client.sldmodel.client.element.type.UserStyle;
import org.ayat.sdi.client.sldmodel.client.element.type.impl.StyledLayerDescriptorImpl;
import org.ayat.sdi.client.sldmodel.client.util.ElementNotFoundException;

import javax.xml.parsers.ParserConfigurationException;


public class SLDModel  {
	public static void main(String[] args) throws ParserConfigurationException, ElementNotFoundException {
		StyledLayerDescriptorImpl sld = new StyledLayerDescriptorImpl();

		sld.setName("sld_name");
		sld.setDescription("sld_desc");

		NamedLayer nl1 = sld.createNamedLayer();
		nl1.setName("nl1_name");
		nl1.setDescription("nl1_desc");
		nl1.setNamedStyle("nl1_ns");

		UserStyle us1 = nl1.createUserStyle();
		us1.setDefault(true);
		us1.setDescription("us1_desc");
		us1.setName("us1_name");

		NamedLayer nl2 = sld.createNamedLayer();
		nl2.setName("nl2_name");
		nl2.setDescription("nl2_desc");
		nl2.setNamedStyle("nl2_ns");

		UserStyle us2 = nl2.createUserStyle();
		us2.setDefault(true);
		us2.setDescription("us2_desc");
		us2.setName("us2_name");

		FeatureTypeStyle fts1 = us2.createFeatureTypeStyle();
		fts1.setAbstract("fts1_abstract");
		fts1.setName("fts1_name");
		fts1.setTitle("fts1_title");

		FeatureTypeStyle fts2 = us2.createFeatureTypeStyle();
		fts2.setAbstract("fts2_abstract");
		fts2.setName("fts2_name");
		fts2.setTitle("fts2_title");
		Fill fill=sld.createNamedLayer().createUserStyle()
				.createFeatureTypeStyle().createRule().createPolygonSymbolizer().createFill();
		fill.setFill("111111");
		fill.setFill("222222");
		fill.setFillOpacity(0.5);
		System.out.println(sld.toString());
	}




}
