package org.ayat.sdi.client.sldmodel.client.util;

public class ElementNotFoundException extends Exception{
	private static final long serialVersionUID = 3787216435659029219L;

	public ElementNotFoundException() {
		super();
	}
	
	public ElementNotFoundException(String message, Throwable cause) {
		super(message, cause);
	}

	public ElementNotFoundException(String message) {
		super(message);
	}

	public ElementNotFoundException(Throwable cause) {
		super(cause);
	}
}
