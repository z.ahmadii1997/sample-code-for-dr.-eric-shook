package org.ayat.sdi.client.sldmodel.client.element.type;

import org.ayat.sdi.client.sldmodel.client.StringPool;
import org.ayat.sdi.client.sldmodel.client.element.aspect.SLDElementHasCSSParameterList;
import org.ayat.sdi.client.sldmodel.client.element.aspect.SLDElementHasGraphicFill;
import org.ayat.sdi.client.sldmodel.client.element.aspect.SLDElementHasGraphicStroke;
import org.ayat.sdi.client.sldmodel.client.util.ElementNotFoundException;

public interface Stroke extends SLDElementHasCSSParameterList,
		SLDElementHasGraphicFill, SLDElementHasGraphicStroke {
	String TAG_NAME = StringPool.SLD_ELEM_STROKE_NAME;

	/**
	 * @throws ElementNotFoundException
	 */
	String getColor() throws ElementNotFoundException;

	void setColor(String rrggbb);

	/**
	 * @throws ElementNotFoundException
	 */
	DashArray getDashArray() throws ElementNotFoundException;

	void setDashArray(DashArray value);

	/**
	 * Removes the dash array if one has been set before.
	 * */
	void removeDashArray();

	/**
	 * Creates a DashArray using the constructor DashArray(Document) and passes
	 * it to setDashArray().
	 * */
	DashArray createDashArray();

	/**
	 * @throws ElementNotFoundException
	 * @throws NumberFormatException
	 */
	Double getOpacity() throws NumberFormatException, ElementNotFoundException;

	void setOpacity(double value);

	/**
	 * @throws ElementNotFoundException
	 * @throws NumberFormatException
	 */
	Integer getWidth() throws NumberFormatException, ElementNotFoundException;

	void setWidth(int value);
}
