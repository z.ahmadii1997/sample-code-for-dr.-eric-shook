package org.ayat.sdi.client.sldmodel.client.element.aspect;

import org.ayat.sdi.client.sldmodel.client.element.SLDElement;
import org.ayat.sdi.client.sldmodel.client.element.type.Mark;
import org.ayat.sdi.client.sldmodel.client.util.ElementNotFoundException;

import org.w3c.dom.*;

public interface SLDElementHasMark extends SLDElement {
	/**
	 * @throws ElementNotFoundException
	 */
	Mark getMark() throws ElementNotFoundException;

	void setMark(Mark value);

	/**
	 * If a {@link Mark} has been set before, removes it. Else does nothing.
	 * */
	void removeMark();

	/**
	 * Creates a {@link Mark} using the constructor {@link Mark}(
	 * {@link Document}) and passes it to {@link #setMark(Mark)}.
	 * */
	Mark createMark();
}
