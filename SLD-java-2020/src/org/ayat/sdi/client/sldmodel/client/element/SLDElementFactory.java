package org.ayat.sdi.client.sldmodel.client.element;

import org.ayat.sdi.client.sldmodel.client.element.type.CSSParameter;
import org.ayat.sdi.client.sldmodel.client.element.type.DashArray;
import org.ayat.sdi.client.sldmodel.client.element.type.ExternalGraphic;
import org.ayat.sdi.client.sldmodel.client.element.type.FeatureTypeStyle;
import org.ayat.sdi.client.sldmodel.client.element.type.Fill;
import org.ayat.sdi.client.sldmodel.client.element.type.Font;
import org.ayat.sdi.client.sldmodel.client.element.type.Graphic;
import org.ayat.sdi.client.sldmodel.client.element.type.GraphicFill;
import org.ayat.sdi.client.sldmodel.client.element.type.GraphicStroke;
import org.ayat.sdi.client.sldmodel.client.element.type.Halo;
import org.ayat.sdi.client.sldmodel.client.element.type.LegendGraphic;
import org.ayat.sdi.client.sldmodel.client.element.type.LineSymbolizer;
import org.ayat.sdi.client.sldmodel.client.element.type.Mark;
import org.ayat.sdi.client.sldmodel.client.element.type.NamedLayer;
import org.ayat.sdi.client.sldmodel.client.element.type.OnlineResource;
import org.ayat.sdi.client.sldmodel.client.element.type.PointSymbolizer;
import org.ayat.sdi.client.sldmodel.client.element.type.PolygonSymbolizer;
import org.ayat.sdi.client.sldmodel.client.element.type.Rule;
import org.ayat.sdi.client.sldmodel.client.element.type.Stroke;
import org.ayat.sdi.client.sldmodel.client.element.type.TextSymbolizer;
import org.ayat.sdi.client.sldmodel.client.element.type.UserStyle;
import org.ayat.sdi.client.sldmodel.client.element.type.impl.CSSParameterImpl;
import org.ayat.sdi.client.sldmodel.client.element.type.impl.DashArrayImpl;
import org.ayat.sdi.client.sldmodel.client.element.type.impl.ExternalGraphicImpl;
import org.ayat.sdi.client.sldmodel.client.element.type.impl.FeatureTypeStyleImpl;
import org.ayat.sdi.client.sldmodel.client.element.type.impl.FillImpl;
import org.ayat.sdi.client.sldmodel.client.element.type.impl.FontImpl;
import org.ayat.sdi.client.sldmodel.client.element.type.impl.GraphicFillImpl;
import org.ayat.sdi.client.sldmodel.client.element.type.impl.GraphicImpl;
import org.ayat.sdi.client.sldmodel.client.element.type.impl.GraphicStrokeImpl;
import org.ayat.sdi.client.sldmodel.client.element.type.impl.HaloImpl;
import org.ayat.sdi.client.sldmodel.client.element.type.impl.LegendGraphicImpl;
import org.ayat.sdi.client.sldmodel.client.element.type.impl.LineSymbolizerImpl;
import org.ayat.sdi.client.sldmodel.client.element.type.impl.MarkImpl;
import org.ayat.sdi.client.sldmodel.client.element.type.impl.NamedLayerImpl;
import org.ayat.sdi.client.sldmodel.client.element.type.impl.OnlineResourceImpl;
import org.ayat.sdi.client.sldmodel.client.element.type.impl.PointSymbolizerImpl;
import org.ayat.sdi.client.sldmodel.client.element.type.impl.PolygonSymbolizerImpl;
import org.ayat.sdi.client.sldmodel.client.element.type.impl.RuleImpl;
import org.ayat.sdi.client.sldmodel.client.element.type.impl.StrokeImpl;
import org.ayat.sdi.client.sldmodel.client.element.type.impl.TextSymbolizerImpl;
import org.ayat.sdi.client.sldmodel.client.element.type.impl.UserStyleImpl;
import org.ayat.sdi.client.sldmodel.client.util.UnknownElementTypeException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;


public class SLDElementFactory {
	/**
	 * Factory method. Doesn't work for StyledLayerDescriptorImpls. Returns null
	 * if class is not recognized.
	 * 
	 * @throws UnknownElementTypeException
	 * */
	@SuppressWarnings("unchecked")
	public static <Type extends SLDElement> Type getInstance(Class<Type> type,
			Document document) throws UnknownElementTypeException {
		if (type.equals(CSSParameter.class))
			return (Type) new CSSParameterImpl(document);
		if (type.equals(DashArray.class))
			return (Type) new DashArrayImpl(document);
		if (type.equals(ExternalGraphic.class))
			return (Type) new ExternalGraphicImpl(document);
		if (type.equals(FeatureTypeStyle.class))
			return (Type) new FeatureTypeStyleImpl(document);
		if (type.equals(Fill.class))
			return (Type) new FillImpl(document);
		if (type.equals(Halo.class))
			return (Type) new HaloImpl(document);
		if (type.equals(Font.class))
			return (Type) new FontImpl(document);
		if (type.equals(GraphicFill.class))
			return (Type) new GraphicFillImpl(document);
		if (type.equals(Graphic.class))
			return (Type) new GraphicImpl(document);
		if (type.equals(GraphicStroke.class))
			return (Type) new GraphicStrokeImpl(document);
		if (type.equals(LegendGraphic.class))
			return (Type) new LegendGraphicImpl(document);
		if (type.equals(LineSymbolizer.class))
			return (Type) new LineSymbolizerImpl(document);
		if (type.equals(Mark.class))
			return (Type) new MarkImpl(document);
		if (type.equals(NamedLayer.class))
			return (Type) new NamedLayerImpl(document);
		if (type.equals(OnlineResource.class))
			return (Type) new OnlineResourceImpl(document);
		if (type.equals(PointSymbolizer.class))
			return (Type) new PointSymbolizerImpl(document);
		if (type.equals(PolygonSymbolizer.class))
			return (Type) new PolygonSymbolizerImpl(document);
		if (type.equals(Rule.class))
			return (Type) new RuleImpl(document);
		if (type.equals(Stroke.class))
			return (Type) new StrokeImpl(document);
		if (type.equals(TextSymbolizer.class))
			return (Type) new TextSymbolizerImpl(document);
		if (type.equals(UserStyle.class))
			return (Type) new UserStyleImpl(document);
		throw new UnknownElementTypeException(type.getName());
	}

	/**
	 * Factory method. Doesn't work for StyledLayerDescriptorImpls. Returns null
	 * if class is not recognized.
	 * 
	 * @throws UnknownElementTypeException
	 * */
	@SuppressWarnings("unchecked")
	public static <Type extends SLDElement> Type getInstance(Class<Type> type,
			Element element) throws UnknownElementTypeException {
		if (type.equals(CSSParameter.class))
			return (Type) new CSSParameterImpl(element);
		if (type.equals(DashArray.class))
			return (Type) new DashArrayImpl(element);
		if (type.equals(ExternalGraphic.class))
			return (Type) new ExternalGraphicImpl(element);
		if (type.equals(FeatureTypeStyle.class))
			return (Type) new FeatureTypeStyleImpl(element);
		if (type.equals(Fill.class))
			return (Type) new FillImpl(element);
		if (type.equals(Halo.class))
			return (Type) new HaloImpl(element);
		if (type.equals(Font.class))
			return (Type) new FontImpl(element);
		if (type.equals(GraphicFill.class))
			return (Type) new GraphicFillImpl(element);
		if (type.equals(Graphic.class))
			return (Type) new GraphicImpl(element);
		if (type.equals(GraphicStroke.class))
			return (Type) new GraphicStrokeImpl(element);
		if (type.equals(LegendGraphic.class))
			return (Type) new LegendGraphicImpl(element);
		if (type.equals(LineSymbolizer.class))
			return (Type) new LineSymbolizerImpl(element);
		if (type.equals(Mark.class))
			return (Type) new MarkImpl(element);
		if (type.equals(NamedLayer.class))
			return (Type) new NamedLayerImpl(element);
		if (type.equals(OnlineResource.class))
			return (Type) new OnlineResourceImpl(element);
		if (type.equals(PointSymbolizer.class))
			return (Type) new PointSymbolizerImpl(element);
		if (type.equals(PolygonSymbolizer.class))
			return (Type) new PolygonSymbolizerImpl(element);
		if (type.equals(Rule.class))
			return (Type) new RuleImpl(element);
		if (type.equals(Stroke.class))
			return (Type) new StrokeImpl(element);
		if (type.equals(TextSymbolizer.class))
			return (Type) new TextSymbolizerImpl(element);
		if (type.equals(UserStyle.class))
			return (Type) new UserStyleImpl(element);
		throw new UnknownElementTypeException(type.getName());
	}
}
