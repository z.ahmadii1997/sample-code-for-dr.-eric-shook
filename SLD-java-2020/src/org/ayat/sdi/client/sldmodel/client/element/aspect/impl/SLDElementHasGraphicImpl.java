package org.ayat.sdi.client.sldmodel.client.element.aspect.impl;

import org.ayat.sdi.client.sldmodel.client.element.AbstractSLDElement;
import org.ayat.sdi.client.sldmodel.client.element.SLDElementFactory;
import org.ayat.sdi.client.sldmodel.client.element.aspect.SLDElementHasGraphic;
import org.ayat.sdi.client.sldmodel.client.element.type.Graphic;
import org.ayat.sdi.client.sldmodel.client.util.ElementNotFoundException;
import org.ayat.sdi.client.sldmodel.client.util.UnknownElementTypeException;

import org.w3c.dom.*;

public abstract class SLDElementHasGraphicImpl extends AbstractSLDElement
		implements SLDElementHasGraphic {
	public SLDElementHasGraphicImpl(Document document, String tagName) {
		super(document, tagName);
	}

	public SLDElementHasGraphicImpl(Element element) {
		super(element);
	}

	@Override
	public Graphic getGraphic() throws ElementNotFoundException {
		try {
			return SLDElementFactory.getInstance(Graphic.class,
					getUniqueChild(Graphic.TAG_NAME));
		} catch (Exception e) {
			throw new ElementNotFoundException(Graphic.TAG_NAME, e);
		}
	}

	@Override
	public void setGraphic(Graphic value) {
		setUniqueChild(value.getElement());
	}

	@Override
	public Graphic createGraphic() {
		try {
			Graphic result = SLDElementFactory.getInstance(Graphic.class,
					document);
			setGraphic(result);
			return result;
		} catch (UnknownElementTypeException e) {// Will never happen
			throw new RuntimeException(e);
		}
	}

	@Override
	public void removeGraphic() {
		try {
			element.removeChild(getGraphic().getElement());
		} catch (ElementNotFoundException e) {
			// Do nothing. there is no graphic to be removed.
		}
	}
}
