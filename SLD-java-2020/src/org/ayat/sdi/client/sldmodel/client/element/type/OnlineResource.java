package org.ayat.sdi.client.sldmodel.client.element.type;

import org.ayat.sdi.client.sldmodel.client.StringPool;
import org.ayat.sdi.client.sldmodel.client.element.SLDElement;

public interface OnlineResource extends SLDElement {
	String TAG_NAME = StringPool.SLD_ELEM_ONLINERESOURCE_NAME;

	String getHREF();

	void setHREF(String value);
}
