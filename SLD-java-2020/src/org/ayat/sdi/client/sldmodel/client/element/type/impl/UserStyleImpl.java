package org.ayat.sdi.client.sldmodel.client.element.type.impl;

import static org.ayat.sdi.client.sldmodel.client.StringPool.SLD_ELEM_USERSTYLE_ELEM_ISDEFAULT_NAME;

import org.ayat.sdi.client.sldmodel.client.element.SLDElementFactory;
import org.ayat.sdi.client.sldmodel.client.element.SLDElementList;
import org.ayat.sdi.client.sldmodel.client.element.aspect.impl.SLDElementHasSelfDescriptionImpl;
import org.ayat.sdi.client.sldmodel.client.element.type.FeatureTypeStyle;
import org.ayat.sdi.client.sldmodel.client.element.type.UserStyle;
import org.ayat.sdi.client.sldmodel.client.util.ElementNotFoundException;
import org.ayat.sdi.client.sldmodel.client.util.UnknownElementTypeException;

import org.w3c.dom.*;

public class UserStyleImpl extends SLDElementHasSelfDescriptionImpl implements
		UserStyle {
	public UserStyleImpl(Document doc) {
		super(doc, UserStyle.TAG_NAME);
	}

	public UserStyleImpl(Element element) {
		super(element);
	}

	@Override
	public Boolean isDefault() throws ElementNotFoundException {
		return getBooleanGrandChildValue(SLD_ELEM_USERSTYLE_ELEM_ISDEFAULT_NAME);
	}

	@Override
	public void setDefault(boolean value) {
		setBooleanGrandChildValue(SLD_ELEM_USERSTYLE_ELEM_ISDEFAULT_NAME, value);
	}

	@Override
	public SLDElementList<FeatureTypeStyle> getFeatureTypeStyles() {
		return new SLDElementList<FeatureTypeStyle>(element
				.getElementsByTagName(FeatureTypeStyle.TAG_NAME),
				FeatureTypeStyle.class);
	}

	@Override
	public void addFeatureTypeStyle(FeatureTypeStyle featureTypeStyle) {
		element.appendChild(featureTypeStyle.getElement());
	}

	@Override
	public void removeFeatureTypeStyle(FeatureTypeStyle featureTypeStyle) {
		element.removeChild(featureTypeStyle.getElement());
	}

	@Override
	public FeatureTypeStyle createFeatureTypeStyle() {
		try {
			FeatureTypeStyle result = SLDElementFactory.getInstance(
					FeatureTypeStyle.class, document);
			addFeatureTypeStyle(result);
			return result;
		} catch (UnknownElementTypeException e) {// Will never happen
			throw new RuntimeException(e);
		}
	}
}
