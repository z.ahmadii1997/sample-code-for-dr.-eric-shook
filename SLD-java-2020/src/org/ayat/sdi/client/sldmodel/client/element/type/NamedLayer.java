package org.ayat.sdi.client.sldmodel.client.element.type;

import org.ayat.sdi.client.sldmodel.client.StringPool;
import org.ayat.sdi.client.sldmodel.client.element.aspect.SLDElementHasSelfDescription;
import org.ayat.sdi.client.sldmodel.client.element.aspect.SLDElementHasUserStyle;
import org.ayat.sdi.client.sldmodel.client.util.ElementNotFoundException;

public interface NamedLayer extends SLDElementHasSelfDescription,
		SLDElementHasUserStyle{
	String TAG_NAME = StringPool.SLD_ELEM_NAMEDLAYER_NAME;

	/**
	 * @throws ElementNotFoundException
	 */
	String getNamedStyle() throws ElementNotFoundException;

	void setNamedStyle(String namedStyle);
}