package org.ayat.sdi.client.sldmodel.client.element.type;

import org.ayat.sdi.client.sldmodel.client.StringPool;
import org.ayat.sdi.client.sldmodel.client.element.aspect.SLDElementHasLineSymbolizerList;
import org.ayat.sdi.client.sldmodel.client.element.aspect.SLDElementHasPointSymbolizerList;
import org.ayat.sdi.client.sldmodel.client.element.aspect.SLDElementHasPolygonSymbolizerList;
import org.ayat.sdi.client.sldmodel.client.element.aspect.SLDElementHasSymbolizerList;
import org.ayat.sdi.client.sldmodel.client.element.aspect.SLDElementHasTextSymbolizerList;
import org.ayat.sdi.client.sldmodel.client.util.ElementNotFoundException;

public interface Rule extends SLDElementHasPointSymbolizerList, SLDElementHasLineSymbolizerList,
		SLDElementHasPolygonSymbolizerList, SLDElementHasTextSymbolizerList, SLDElementHasSymbolizerList {
	String TAG_NAME = StringPool.SLD_ELEM_RULE_NAME;

	/**
	 * @throws ElementNotFoundException
	 */
	String getName() throws ElementNotFoundException;

	void setName(String value);

	/**
	 * @throws ElementNotFoundException
	 */
	String getTitle() throws ElementNotFoundException;

	void setTitle(String value);

	/**
	 * @throws ElementNotFoundException
	 */
	String getAbstract() throws ElementNotFoundException;

	void setAbstract(String value);

	/**
	 * @throws ElementNotFoundException
	 * @throws NumberFormatException
	 */
	Double getMinScaleDenominator() throws NumberFormatException,
			ElementNotFoundException;

	void setMinScaleDenominator(Double value);

	/**
	 * @throws ElementNotFoundException
	 * @throws NumberFormatException
	 */
	Double getMaxScaleDenominator() throws NumberFormatException,
			ElementNotFoundException;

	void setMaxScaleDenominator(Double value);
}