package org.ayat.sdi.client.sldmodel.client.element;

import org.ayat.sdi.client.sldmodel.client.util.UnknownElementTypeException;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;


public class SLDElementList<NodeWrapperType extends SLDElement> {
	NodeList nodeList;
	Class<NodeWrapperType> nodeWrapperClass;

	public SLDElementList(NodeList nodeList,
			Class<NodeWrapperType> nodeWrapperClass) {
		this.nodeList = nodeList;
		this.nodeWrapperClass = nodeWrapperClass;
	}

	public NodeWrapperType getItem(int index)
			throws UnknownElementTypeException {
		return SLDElementFactory.<NodeWrapperType> getInstance(
				nodeWrapperClass, (Element) nodeList.item(index));
	}

	public final int getLength() {
		return nodeList.getLength();
	}
}