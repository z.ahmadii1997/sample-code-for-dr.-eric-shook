package org.ayat.sdi.client.sldmodel.client.core;

import java.util.ArrayList;



public class Utility {
	/**
	 * Deletes the last occurrence of toDelete if string is not empty and
	 * contains toDelete.
	 * */
	public static void deleteTrailing(StringBuffer string, String toDelete) {
		if (toDelete.isEmpty())
			return;
		int index = string.lastIndexOf(toDelete);
		if (string.length() > 0 && index != -1)// delete the trailing
			string.delete(index, index + toDelete.length());
	}
	public static String getSimpleClassName(Class<?> c) {
		String result=c.getName();
		return result.substring(result.lastIndexOf(StringPool.DOT)+1,result.length());
	}
	/**
	 * <pre>
	 * messagePriority values:
	 * 		o-4, Robust
	 * 		o-3, Informational
	 * 		o-2, Warning
	 * 		o-1, Critical
	 * </pre>
	 * */
	public static void log(Object you, int messagePriority, String message){
		if(messagePriority<=StringPool.debugLevel){
			String messageType="";
			switch(messagePriority){
			case 4:
				messageType="[VERB]";
				break;
			case 3:
				messageType="[INFO]";
				break;
			case 2:
				messageType="[WARN]";
				break;
			case 1:
				messageType="[ERROR]";
				break;
			}
			
			System.out.println(messageType+StringPool.SPACE+getSimpleClassName(you.getClass())+StringPool.COLON+StringPool.SPACE+message);
		}
	}
	public static String createQueryURL(String baseURL,NameValuePair... params){
		StringBuffer result=new StringBuffer();
		result.append(baseURL);
		
		result.append(StringPool.QUESTION_MARK);
		for(NameValuePair param:params){
			result.append(param);
			result.append(StringPool.AMPERSAND);
		}
		return result.toString();
	}

	

	

	public static String getURLParamName(Class<?> c) {
		return toParamNameCase(Utility.getSimpleClassName(c));
	}
	
	public static String toParamNameCase(String string) {
		return string.toLowerCase();
	}

	/**
	 * Combines the given array lists into one with the given super class type.
	 * */
	public static <SuperClass> ArrayList<SuperClass> combine(
			ArrayList<? extends SuperClass>... arrayLists) {
		ArrayList<SuperClass> result = new ArrayList<SuperClass>();
		for (ArrayList<? extends SuperClass> arrayList : arrayLists)
			for (SuperClass element : arrayList)
				result.add(element);
		return result;
	}
}