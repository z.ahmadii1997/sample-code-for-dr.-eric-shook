package org.ayat.sdi.client.sldmodel.client.element.aspect.impl;

import java.util.ArrayList;

import org.ayat.sdi.client.sldmodel.client.element.AbstractSLDElement;
import org.ayat.sdi.client.sldmodel.client.element.SLDElementCollection;
import org.ayat.sdi.client.sldmodel.client.element.SLDElementFactory;
import org.ayat.sdi.client.sldmodel.client.element.SLDElementList;
import org.ayat.sdi.client.sldmodel.client.element.aspect.SLDElementHasLineSymbolizerList;
import org.ayat.sdi.client.sldmodel.client.element.aspect.SLDElementHasPointSymbolizerList;
import org.ayat.sdi.client.sldmodel.client.element.aspect.SLDElementHasPolygonSymbolizerList;
import org.ayat.sdi.client.sldmodel.client.element.aspect.SLDElementHasSymbolizerList;
import org.ayat.sdi.client.sldmodel.client.element.aspect.SLDElementHasTextSymbolizerList;
import org.ayat.sdi.client.sldmodel.client.element.type.LineSymbolizer;
import org.ayat.sdi.client.sldmodel.client.element.type.PointSymbolizer;
import org.ayat.sdi.client.sldmodel.client.element.type.PolygonSymbolizer;
import org.ayat.sdi.client.sldmodel.client.element.type.Symbolizer;
import org.ayat.sdi.client.sldmodel.client.element.type.TextSymbolizer;
import org.ayat.sdi.client.sldmodel.client.util.UnknownElementTypeException;

import org.w3c.dom.*;

public abstract class SLDElementHasSymbolizersImpl extends
		AbstractSLDElement implements SLDElementHasPointSymbolizerList,
		SLDElementHasLineSymbolizerList, SLDElementHasPolygonSymbolizerList, SLDElementHasTextSymbolizerList,
		SLDElementHasSymbolizerList {
	protected ArrayList<PointSymbolizer> pointSymbolizerList = new ArrayList<PointSymbolizer>();
	protected ArrayList<LineSymbolizer> lineSymbolizerList = new ArrayList<LineSymbolizer>();
	protected ArrayList<PolygonSymbolizer> polygonSymbolizerList = new ArrayList<PolygonSymbolizer>();
	protected ArrayList<TextSymbolizer> textSymbolizerList = new ArrayList<TextSymbolizer>();

	public SLDElementHasSymbolizersImpl(Document document, String tagName) {
		super(document, tagName);
	}

	public SLDElementHasSymbolizersImpl(Element element) {
		super(element);
		parsePointSymbolizers();
		parseLineSymbolizers();
		parsePolygonSymbolizers();
		parseTextSymbolizers();
	}

	protected void parsePointSymbolizers() {
		try {
			pointSymbolizerList.clear();
			SLDElementList<PointSymbolizer> nodeWrapperList = new SLDElementList<PointSymbolizer>(
					element.getElementsByTagName(PointSymbolizer.TAG_NAME),
					PointSymbolizer.class);
			for (int i = 0; i < nodeWrapperList.getLength(); i++)
				pointSymbolizerList.add(nodeWrapperList.getItem(i));
		} catch (UnknownElementTypeException e) {
			throw new RuntimeException(e);
		}
	}

	protected void parseLineSymbolizers() {
		try {
			lineSymbolizerList.clear();
			SLDElementList<LineSymbolizer> nodeWrapperList = new SLDElementList<LineSymbolizer>(
					element.getElementsByTagName(LineSymbolizer.TAG_NAME),
					LineSymbolizer.class);
			for (int i = 0; i < nodeWrapperList.getLength(); i++)
				lineSymbolizerList.add(nodeWrapperList.getItem(i));
		} catch (UnknownElementTypeException e) {
			throw new RuntimeException(e);
		}
	}

	protected void parsePolygonSymbolizers() {
		try {
			polygonSymbolizerList.clear();
			SLDElementList<PolygonSymbolizer> nodeWrapperList = new SLDElementList<PolygonSymbolizer>(
					element.getElementsByTagName(PolygonSymbolizer.TAG_NAME),
					PolygonSymbolizer.class);
			for (int i = 0; i < nodeWrapperList.getLength(); i++)
				polygonSymbolizerList.add(nodeWrapperList.getItem(i));
		} catch (UnknownElementTypeException e) {
			throw new RuntimeException(e);
		}
	}

	protected void parseTextSymbolizers() {
		try {
			textSymbolizerList.clear();
			SLDElementList<TextSymbolizer> nodeWrapperList = new SLDElementList<TextSymbolizer>(
					element.getElementsByTagName(TextSymbolizer.TAG_NAME),
					TextSymbolizer.class);
			for (int i = 0; i < nodeWrapperList.getLength(); i++)
				textSymbolizerList.add(nodeWrapperList.getItem(i));
		} catch (UnknownElementTypeException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public void addPointSymbolizer(PointSymbolizer value) {
		pointSymbolizerList.add(value);
		element.appendChild(value.getElement());
	}

	@Override
	public void removePointSymbolizer(PointSymbolizer value) {
		element.removeChild(value.getElement());
		pointSymbolizerList.remove(value);
	}

	@Override
	public SLDElementCollection<PointSymbolizer> getPointSymbolizers() {
		return new SLDElementCollection<PointSymbolizer>(pointSymbolizerList,
				PointSymbolizer.class);
	}

	@Override
	public PointSymbolizer getPointSymbolizer(int index) {
		return pointSymbolizerList.get(index);
	}

	@Override
	public void removePointSymbolizers() {
		for (PointSymbolizer pointSymbolizer : pointSymbolizerList)
			removePointSymbolizer(pointSymbolizer);
	}

	@Override
	public void addLineSymbolizer(LineSymbolizer value) {
		lineSymbolizerList.add(value);
		element.appendChild(value.getElement());
	}

	@Override
	public void removeLineSymbolizer(LineSymbolizer value) {
		element.removeChild(value.getElement());
		lineSymbolizerList.remove(value);
	}

	@Override
	public SLDElementCollection<LineSymbolizer> getLineSymbolizers() {
		return new SLDElementCollection<LineSymbolizer>(lineSymbolizerList,
				LineSymbolizer.class);
	}

	@Override
	public LineSymbolizer getLineSymbolizer(int index) {
		return lineSymbolizerList.get(index);
	}

	@Override
	public void removeLineSymbolizers() {
		for (LineSymbolizer lineSymbolizer : lineSymbolizerList)
			removeLineSymbolizer(lineSymbolizer);
	}

	@Override
	public void addPolygonSymbolizer(PolygonSymbolizer value) {
		polygonSymbolizerList.add(value);
		element.appendChild(value.getElement());
	}

	@Override
	public void removePolygonSymbolizer(PolygonSymbolizer value) {
		element.removeChild(value.getElement());
		polygonSymbolizerList.remove(value);
	}

	@Override
	public SLDElementCollection<PolygonSymbolizer> getPolygonSymbolizers() {
		return new SLDElementCollection<PolygonSymbolizer>(
				polygonSymbolizerList, PolygonSymbolizer.class);
	}

	@Override
	public PolygonSymbolizer getPolygonSymbolizer(int index) {
		return polygonSymbolizerList.get(index);
	}

	@Override
	public void removePolygonSymbolizers() {
		for (PolygonSymbolizer polygonSymbolizer : polygonSymbolizerList)
			removePolygonSymbolizer(polygonSymbolizer);
	}

	@Override
	public void addTextSymbolizer(TextSymbolizer value) {
		textSymbolizerList.add(value);
		element.appendChild(value.getElement());
	}

	@Override
	public void removeTextSymbolizer(TextSymbolizer value) {
		element.removeChild(value.getElement());
		textSymbolizerList.remove(value);
	}

	@Override
	public SLDElementCollection<TextSymbolizer> getTextSymbolizers() {
		return new SLDElementCollection<TextSymbolizer>(textSymbolizerList,
				TextSymbolizer.class);
	}

	@Override
	public TextSymbolizer getTextSymbolizer(int index) {
		return textSymbolizerList.get(index);
	}

	@Override
	public void removeTextSymbolizers() {
		for (TextSymbolizer textSymbolizer : textSymbolizerList)
			removeTextSymbolizer(textSymbolizer);
	}

	@Override
	public PointSymbolizer createPointSymbolizer() {
		PointSymbolizer result;
		try {
			result = SLDElementFactory.getInstance(PointSymbolizer.class,
					document);
			addPointSymbolizer(result);
			return result;
		} catch (UnknownElementTypeException e) {// Will never happen
			throw new RuntimeException(e);
		}
	}

	@Override
	public LineSymbolizer createLineSymbolizer() {
		try {
			LineSymbolizer result = SLDElementFactory.getInstance(
					LineSymbolizer.class, document);
			addLineSymbolizer(result);
			return result;
		} catch (UnknownElementTypeException e) {// Will never happen
			throw new RuntimeException(e);
		}
	}

	@Override
	public PolygonSymbolizer createPolygonSymbolizer() {
		try {
			PolygonSymbolizer result = SLDElementFactory.getInstance(
					PolygonSymbolizer.class, document);
			addPolygonSymbolizer(result);
			return result;
		} catch (UnknownElementTypeException e) {// Will never happen
			throw new RuntimeException(e);
		}
	}

	@Override
	public TextSymbolizer createTextSymbolizer() {
		try {
			TextSymbolizer result = SLDElementFactory.getInstance(
					TextSymbolizer.class, document);
			addTextSymbolizer(result);
			return result;
		} catch (UnknownElementTypeException e) {// Will never happen
			throw new RuntimeException(e);
		}
	}

	@SuppressWarnings("unchecked")


	@Override
	public void removeSymbolizers() {
		removeLineSymbolizers();
		removePointSymbolizers();
		removePolygonSymbolizers();
		removeTextSymbolizers();
	}
}
