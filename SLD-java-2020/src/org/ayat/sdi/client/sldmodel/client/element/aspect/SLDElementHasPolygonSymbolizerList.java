package org.ayat.sdi.client.sldmodel.client.element.aspect;

import org.ayat.sdi.client.sldmodel.client.element.SLDElement;
import org.ayat.sdi.client.sldmodel.client.element.SLDElementCollection;
import org.ayat.sdi.client.sldmodel.client.element.type.PolygonSymbolizer;

import org.w3c.dom.*;

public interface SLDElementHasPolygonSymbolizerList extends SLDElement {
	/**
	 * Adds a {@link PolygonSymbolizer}.
	 * */
	void addPolygonSymbolizer(PolygonSymbolizer value);

	/**
	 * Returns the {@link PolygonSymbolizer} at the specified index.
	 * */
	PolygonSymbolizer getPolygonSymbolizer(int index);

	/**
	 * Removes the specified {@link PolygonSymbolizer}.
	 * */
	void removePolygonSymbolizer(PolygonSymbolizer value);

	/**
	 * Returns all {@link PolygonSymbolizer}s.
	 * */
	SLDElementCollection<PolygonSymbolizer> getPolygonSymbolizers();

	/**
	 * Removes all {@link PolygonSymbolizer}s.
	 * */
	void removePolygonSymbolizers();

	/**
	 * Creates a {@link PolygonSymbolizer} using the constructor
	 * {@link PolygonSymbolizer}({@link Document}) and passes it to
	 * {@link #addPolygonSymbolizer(PolygonSymbolizer)}.
	 * */
	PolygonSymbolizer createPolygonSymbolizer();
}