package org.ayat.sdi.client.sldmodel.client.element.aspect;

import org.ayat.sdi.client.sldmodel.client.element.SLDElement;
import org.ayat.sdi.client.sldmodel.client.element.SLDElementList;
import org.ayat.sdi.client.sldmodel.client.element.type.Rule;

import org.w3c.dom.*;

public interface SLDElementHasRuleList extends SLDElement {
	void addRule(Rule rule);

	void removeRule(Rule rule);

	SLDElementList<Rule> getRuleList();

	/**
	 * Creates a {@link Rule} using the constructor {@link Rule}(
	 * {@link Document}) and passes it to {@link #addRule(Rule)}.
	 * */
	Rule createRule();
}
