package org.ayat.sdi.client.sldmodel.client.element.type.impl;

import static org.ayat.sdi.client.sldmodel.client.StringPool.SLD_ELEM_ROOT_ATTR_VERSION_NAME;
import static org.ayat.sdi.client.sldmodel.client.StringPool.SLD_ELEM_ROOT_NAME;
import static org.ayat.sdi.client.sldmodel.client.StringPool.XML_ELEM_ANY_ATTR_XMLNS_NAME;
import static org.ayat.sdi.client.sldmodel.client.StringPool.XML_ELEM_ANY_ATTR_XSI_SCHEMA_LOC_NAME;
import static org.ayat.sdi.client.sldmodel.client.StringPool.XML_ELEM_ANY_ATTR_XSI_SCHEMA_LOC_VAL_SLD;
import static org.ayat.sdi.client.sldmodel.client.StringPool.XML_NS_PFIX_OPENGIS_NET_OGC;
import static org.ayat.sdi.client.sldmodel.client.StringPool.XML_NS_PFIX_W3_ORG_01_XSI;
import static org.ayat.sdi.client.sldmodel.client.StringPool.XML_NS_PFIX_W3_ORG_99_XLINK;
import static org.ayat.sdi.client.sldmodel.client.StringPool.XML_NS_URI_OPENGIS_NET_OGC;
import static org.ayat.sdi.client.sldmodel.client.StringPool.XML_NS_URI_OPENGIS_NET_SLD;
import static org.ayat.sdi.client.sldmodel.client.StringPool.XML_NS_URI_W3_ORG_01_XSI;
import static org.ayat.sdi.client.sldmodel.client.StringPool.XML_NS_URI_W3_ORG_99_XLINK;
import static  org.ayat.sdi.client.sldmodel.client.core.StringPool.COLON;

import org.ayat.sdi.client.sldmodel.client.StringPool.SLD_ELEM_ROOT_ATTR_VERSION_VAL;
import org.ayat.sdi.client.sldmodel.client.element.SLDElementFactory;
import org.ayat.sdi.client.sldmodel.client.element.SLDElementList;
import org.ayat.sdi.client.sldmodel.client.element.aspect.impl.SLDElementHasSelfDescriptionImpl;
import org.ayat.sdi.client.sldmodel.client.element.type.NamedLayer;
import org.ayat.sdi.client.sldmodel.client.element.type.StyledLayerDescriptor;
import org.ayat.sdi.client.sldmodel.client.util.UnknownElementTypeException;

import org.w3c.dom.*;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

public class StyledLayerDescriptorImpl extends SLDElementHasSelfDescriptionImpl
		implements StyledLayerDescriptor {
	public StyledLayerDescriptorImpl() throws ParserConfigurationException {
		super(DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument(), SLD_ELEM_ROOT_NAME);
	}

	@Override
	protected void doInitElement() {
		document.appendChild(element);
		setVersion(SLD_ELEM_ROOT_ATTR_VERSION_VAL.V1_0_0);
		
		element.setAttribute(XML_ELEM_ANY_ATTR_XMLNS_NAME+COLON+"se",
				XML_NS_URI_OPENGIS_NET_SLD);
		
		element.setAttribute(XML_ELEM_ANY_ATTR_XMLNS_NAME + COLON
				+ XML_NS_PFIX_OPENGIS_NET_OGC, XML_NS_URI_OPENGIS_NET_OGC);
		
		element.setAttribute(XML_ELEM_ANY_ATTR_XMLNS_NAME + COLON
				+ XML_NS_PFIX_W3_ORG_99_XLINK, XML_NS_URI_W3_ORG_99_XLINK);
		
		element.setAttribute(XML_ELEM_ANY_ATTR_XMLNS_NAME + COLON
				+ XML_NS_PFIX_W3_ORG_01_XSI, XML_NS_URI_W3_ORG_01_XSI);
		
		element.setAttribute(XML_ELEM_ANY_ATTR_XSI_SCHEMA_LOC_NAME,
				XML_ELEM_ANY_ATTR_XSI_SCHEMA_LOC_VAL_SLD);
	}

	@Override
	public SLD_ELEM_ROOT_ATTR_VERSION_VAL getVersion() {
		return SLD_ELEM_ROOT_ATTR_VERSION_VAL.parse(element
				.getAttribute(SLD_ELEM_ROOT_ATTR_VERSION_NAME));
	}

	@Override
	public void setVersion(SLD_ELEM_ROOT_ATTR_VERSION_VAL version) {
		element.setAttribute(SLD_ELEM_ROOT_ATTR_VERSION_NAME, version
				.toString());
	}

	@Override
	public SLDElementList<NamedLayer> getNamedLayers() {
		return new SLDElementList<NamedLayer>(element
				.getElementsByTagName(NamedLayer.TAG_NAME), NamedLayer.class);
	}

	@Override
	public void addNamedLayer(NamedLayer namedLayer) {
		element.appendChild(namedLayer.getElement());
	}

	@Override
	public NamedLayer createNamedLayer() {
		try {
			NamedLayer result = SLDElementFactory.getInstance(NamedLayer.class,
					document);
			addNamedLayer(result);
			return result;
		} catch (UnknownElementTypeException e) {// Will never happen
			throw new RuntimeException(e);
		}
	}

	@Override
	public void removeNamedLayer(NamedLayer namedLayer) {
		element.removeChild(namedLayer.getElement());
	}
}
