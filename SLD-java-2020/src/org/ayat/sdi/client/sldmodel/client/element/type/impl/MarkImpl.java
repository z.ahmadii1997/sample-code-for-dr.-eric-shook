package org.ayat.sdi.client.sldmodel.client.element.type.impl;

import org.ayat.sdi.client.sldmodel.client.StringPool;
import org.ayat.sdi.client.sldmodel.client.StringPool.SLD_ELEM_MARK_ATTR_WELLKNOWNNAME_VAL;
import org.ayat.sdi.client.sldmodel.client.element.aspect.impl.SLDElementHasFillHasStrokeImpl;
import org.ayat.sdi.client.sldmodel.client.element.type.Mark;
import org.ayat.sdi.client.sldmodel.client.util.ElementNotFoundException;

import org.w3c.dom.*;

public class MarkImpl extends SLDElementHasFillHasStrokeImpl implements
		Mark {
	public MarkImpl(Document document) {
		super(document, Mark.TAG_NAME);
	}

	public MarkImpl(Element element) {
		super(element);
	}

	@Override
	public SLD_ELEM_MARK_ATTR_WELLKNOWNNAME_VAL getWellKnownName()
			throws ElementNotFoundException {
		return SLD_ELEM_MARK_ATTR_WELLKNOWNNAME_VAL
				.parse(getTextGrandChildValue(StringPool.SLD_ELEM_MARK_ELEM_WELLKNOWNNAME_NAME));
	}

	@Override
	public void setWellKnownName(SLD_ELEM_MARK_ATTR_WELLKNOWNNAME_VAL value) {
		setTextGrandChildValue(
				StringPool.SLD_ELEM_MARK_ELEM_WELLKNOWNNAME_NAME, value
						.toString());
	}

	@Override
	public void setWellKnownName(String value) {
		setTextGrandChildValue(
				StringPool.SLD_ELEM_MARK_ELEM_WELLKNOWNNAME_NAME, value
						.toString());
		
	}
}
