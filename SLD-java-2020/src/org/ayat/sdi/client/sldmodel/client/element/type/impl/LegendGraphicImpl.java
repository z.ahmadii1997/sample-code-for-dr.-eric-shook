package org.ayat.sdi.client.sldmodel.client.element.type.impl;

import org.ayat.sdi.client.sldmodel.client.element.aspect.impl.SLDElementHasGraphicImpl;
import org.ayat.sdi.client.sldmodel.client.element.type.LegendGraphic;

import org.w3c.dom.*;

public class LegendGraphicImpl extends SLDElementHasGraphicImpl implements
		LegendGraphic {
	public LegendGraphicImpl(Document document) {
		super(document, LegendGraphic.TAG_NAME);
	}

	public LegendGraphicImpl(Element element) {
		super(element);
	}
}
