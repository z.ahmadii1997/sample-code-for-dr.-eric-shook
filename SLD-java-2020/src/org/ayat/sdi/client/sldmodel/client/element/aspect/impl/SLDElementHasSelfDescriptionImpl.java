package org.ayat.sdi.client.sldmodel.client.element.aspect.impl;

import static org.ayat.sdi.client.sldmodel.client.StringPool.SLD_ELEM_ANY_ELEM_DESC_NAME;
import static org.ayat.sdi.client.sldmodel.client.StringPool.SLD_ELEM_ANY_ELEM_NAME_NAME;

import org.ayat.sdi.client.sldmodel.client.element.AbstractSLDElement;
import org.ayat.sdi.client.sldmodel.client.element.aspect.SLDElementHasSelfDescription;
import org.ayat.sdi.client.sldmodel.client.util.ElementNotFoundException;

import org.w3c.dom.*;

public abstract class SLDElementHasSelfDescriptionImpl extends
		AbstractSLDElement implements SLDElementHasSelfDescription{
	public SLDElementHasSelfDescriptionImpl(Document document, String tagName) {
		super(document, tagName);
	}

	public SLDElementHasSelfDescriptionImpl(Element element) {
		super(element);
	}
	
	@Override
	public String getDescription() throws ElementNotFoundException {
		return getTextGrandChildValue(SLD_ELEM_ANY_ELEM_DESC_NAME);
	}

	@Override
	public String getName() throws ElementNotFoundException {
		return getTextGrandChildValue(SLD_ELEM_ANY_ELEM_NAME_NAME);
	}

	@Override
	public void setDescription(String description) {
		setTextGrandChildValue(SLD_ELEM_ANY_ELEM_DESC_NAME, description);
	}

	@Override
	public void setName(String name) {
		setTextGrandChildValue(SLD_ELEM_ANY_ELEM_NAME_NAME, name);
	}
}
