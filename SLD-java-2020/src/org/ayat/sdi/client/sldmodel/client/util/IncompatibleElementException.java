package org.ayat.sdi.client.sldmodel.client.util;

public class IncompatibleElementException extends Exception{
	private static final long serialVersionUID = 3787216435659029219L;

	public IncompatibleElementException() {
		super();
	}
	
	public IncompatibleElementException(String message, Throwable cause) {
		super(message, cause);
	}

	public IncompatibleElementException(String message) {
		super(message);
	}

	public IncompatibleElementException(Throwable cause) {
		super(cause);
	}
}
