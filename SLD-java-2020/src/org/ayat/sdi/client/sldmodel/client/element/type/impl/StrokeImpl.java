package org.ayat.sdi.client.sldmodel.client.element.type.impl;

import org.ayat.sdi.client.sldmodel.client.StringPool;
import org.ayat.sdi.client.sldmodel.client.element.SLDElementFactory;
import org.ayat.sdi.client.sldmodel.client.element.aspect.impl.SLDElementHasCSSParamsImpl;
import org.ayat.sdi.client.sldmodel.client.element.type.DashArray;
import org.ayat.sdi.client.sldmodel.client.element.type.GraphicFill;
import org.ayat.sdi.client.sldmodel.client.element.type.GraphicStroke;
import org.ayat.sdi.client.sldmodel.client.element.type.Stroke;
import org.ayat.sdi.client.sldmodel.client.util.ElementNotFoundException;
import org.ayat.sdi.client.sldmodel.client.util.UnknownElementTypeException;

import org.w3c.dom.*;

public class StrokeImpl extends SLDElementHasCSSParamsImpl implements Stroke {
	public StrokeImpl(Document document) {
		super(document, Stroke.TAG_NAME);
	}

	public StrokeImpl(Element element) {
		super(element);
	}

	@Override
	public String getColor() throws ElementNotFoundException {
		return getTextGrandChildValue(StringPool.SLD_ELEM_ANY_ELEM_COLOR_NAME);
	}

	@Override
	public DashArray getDashArray() throws ElementNotFoundException {
		try {
			return SLDElementFactory.getInstance(DashArray.class,
					getUniqueChild(DashArray.TAG_NAME));
		} catch (UnknownElementTypeException e) {// Will never happen
			throw new RuntimeException(e);
		}
	}

	@Override
	public GraphicFill getGraphicFill() throws ElementNotFoundException {
		try {
			return SLDElementFactory.getInstance(GraphicFill.class,
					getUniqueChild(GraphicFill.TAG_NAME));
		} catch (UnknownElementTypeException e) {// Will never happen
			throw new RuntimeException(e);
		}
	}

	@Override
	public GraphicStroke getGraphicStroke() throws ElementNotFoundException {
		try {
			return SLDElementFactory.getInstance(GraphicStroke.class,
					getUniqueChild(GraphicStroke.TAG_NAME));
		} catch (UnknownElementTypeException e) {// Will never happen
			throw new RuntimeException(e);
		}
	}

	@Override
	public Double getOpacity() throws NumberFormatException,
			ElementNotFoundException {
		return getDoubleGrandChildValue(StringPool.SLD_ELEM_ANY_ELEM_OPACITY_NAME);
	}

	@Override
	public Integer getWidth() throws NumberFormatException,
			ElementNotFoundException {
		return getIntegerGrandChildValue(StringPool.SLD_ELEM_ANY_ELEM_WIDTH_NAME);
	}

	@Override
	public void setColor(String rrggbb) {
		setUniqueOGCLiteral(new CSSParameterImpl(document,
				StringPool.CSS_PARAM_STROKE_NAME), StringPool.SHARP + rrggbb);
	}

	@Override
	public void setDashArray(DashArray value) {
		setUniqueChild(value.getElement());
	}

	@Override
	public void setGraphicFill(GraphicFill value) {
		setUniqueChild(value.getElement());
	}

	@Override
	public void setGraphicStroke(GraphicStroke value) {
		setUniqueChild(value.getElement());
	}

	@Override
	public void setOpacity(double value) {
		setUniqueOGCLiteral(new CSSParameterImpl(document,
				StringPool.CSS_PARAM_OPACITY_NAME), Double.toString(value));
	}

	@Override
	public void setWidth(int value) {
		setUniqueOGCLiteral(new CSSParameterImpl(document,
				StringPool.CSS_PARAM_STROKEWIDTH_NAME), Integer.toString(value));
	}

	@Override
	public GraphicFill createGraphicFill() {
		try {
			GraphicFill result = SLDElementFactory.getInstance(
					GraphicFill.class, document);
			setGraphicFill(result);
			return result;
		} catch (UnknownElementTypeException e) {// Will never happen
			throw new RuntimeException(e);
		}
	}

	@Override
	public GraphicStroke createGraphicStroke() {
		try {
			GraphicStroke result = SLDElementFactory.getInstance(
					GraphicStroke.class, document);
			setGraphicStroke(result);
			return result;
		} catch (UnknownElementTypeException e) {// Will never happen
			throw new RuntimeException(e);
		}
	}

	@Override
	public DashArray createDashArray() {
		try {
			DashArray result = SLDElementFactory.getInstance(DashArray.class,
					document);
			setDashArray(result);
			return result;
		} catch (UnknownElementTypeException e) {// Will never happen
			throw new RuntimeException(e);
		}
	}

	@Override
	public void removeDashArray() {
		try {
			element.removeChild(getDashArray().getElement());
		} catch (ElementNotFoundException e) {
			// Do nothing. There is no dash array set to be removed.
		}
	}

	@Override
	public void removeGraphicFill() {
		try {
			element.removeChild(getGraphicFill().getElement());
		} catch (ElementNotFoundException e) {
			// Nothing to remove.
		}
	}

	@Override
	public void removeGraphicStroke() {
		try {
			element.removeChild(getGraphicStroke().getElement());
		} catch (ElementNotFoundException e) {
			// Nothing to remove.
		}
	}
}
