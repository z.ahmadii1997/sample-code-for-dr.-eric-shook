package org.ayat.sdi.client.sldmodel.client.element.aspect;

import java.util.ArrayList;

import org.ayat.sdi.client.sldmodel.client.element.SLDElement;
import org.ayat.sdi.client.sldmodel.client.element.type.Symbolizer;

public interface SLDElementHasSymbolizerList extends SLDElement {
	/**
	 * Returns all {@link Symbolizer}s.
	 * */
	ArrayList<Symbolizer> getSymbolizers();

	/**
	 * Removes all {@link Symbolizer}s.
	 * */
	void removeSymbolizers();
}