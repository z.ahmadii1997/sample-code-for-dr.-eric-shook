package org.ayat.sdi.client.sldmodel.client;


public class StringPool extends org.ayat.sdi.client.sldmodel.client.core.StringPool {
	public static enum CSS_PARAM_FONTSTYLE_VAL {
		Italic, Normal;
		public static CSS_PARAM_FONTSTYLE_VAL parse(String value) {
			CSS_PARAM_FONTSTYLE_VAL result = null;
			if (value.equals(CSS_PARAM_FONTSTYLE_VAL_ITALIC))
				result = CSS_PARAM_FONTSTYLE_VAL.Italic;
			if (value.equals(CSS_PARAM_FONTSTYLE_VAL_NORMAL))
				result = CSS_PARAM_FONTSTYLE_VAL.Normal;
			return result;
		}

		public String toString() {
			String result = "";
			switch (this) {
			case Italic:
				result = CSS_PARAM_FONTSTYLE_VAL_ITALIC;
				break;
			case Normal:
				result = CSS_PARAM_FONTSTYLE_VAL_NORMAL;
				break;
			}
			return result;
		}
	}

	public static enum CSS_PARAM_FONTWEIGHT_VAL {
		Bold, Normal;
		public static CSS_PARAM_FONTWEIGHT_VAL parse(String value) {
			CSS_PARAM_FONTWEIGHT_VAL result = null;
			if (value.equals(CSS_PARAM_FONTWEIGHT_VAL_BOLD))
				result = CSS_PARAM_FONTWEIGHT_VAL.Bold;
			if (value.equals(CSS_PARAM_FONTWEIGHT_VAL_NORMAL))
				result = CSS_PARAM_FONTWEIGHT_VAL.Normal;
			return result;
		}

		public String toString() {
			String result = "";
			switch (this) {
			case Bold:
				result = CSS_PARAM_FONTWEIGHT_VAL_BOLD;
				break;
			case Normal:
				result = CSS_PARAM_FONTWEIGHT_VAL_NORMAL;
				break;
			}
			return result;
		}
	}

	public static enum SLD_ELEM_MARK_ATTR_WELLKNOWNNAME_VAL {
		CIRCLE, CROSS, SQUARE, STAR, TRIANGLE, X;
		public static SLD_ELEM_MARK_ATTR_WELLKNOWNNAME_VAL parse(String value) {
			SLD_ELEM_MARK_ATTR_WELLKNOWNNAME_VAL result = null;
			if (value.equals(SLD_ELEM_MARK_ATTR_WELLKNOWNNAME_VAL_CIRCLE))
				result = SLD_ELEM_MARK_ATTR_WELLKNOWNNAME_VAL.CIRCLE;
			if (value.equals(SLD_ELEM_MARK_ATTR_WELLKNOWNNAME_VAL_CROSS))
				result = SLD_ELEM_MARK_ATTR_WELLKNOWNNAME_VAL.CROSS;
			if (value.equals(SLD_ELEM_MARK_ATTR_WELLKNOWNNAME_VAL_SQUARE))
				result = SLD_ELEM_MARK_ATTR_WELLKNOWNNAME_VAL.SQUARE;
			if (value.equals(SLD_ELEM_MARK_ATTR_WELLKNOWNNAME_VAL_STAR))
				result = SLD_ELEM_MARK_ATTR_WELLKNOWNNAME_VAL.STAR;
			if (value.equals(SLD_ELEM_MARK_ATTR_WELLKNOWNNAME_VAL_TRIANGLE))
				result = SLD_ELEM_MARK_ATTR_WELLKNOWNNAME_VAL.TRIANGLE;
			if (value.equals(SLD_ELEM_MARK_ATTR_WELLKNOWNNAME_VAL_X))
				result = SLD_ELEM_MARK_ATTR_WELLKNOWNNAME_VAL.X;
			return result;
		}

		public String toString() {
			String result = null;
			switch (this) {
			case CIRCLE:
				result = SLD_ELEM_MARK_ATTR_WELLKNOWNNAME_VAL_CIRCLE;
				break;
			case CROSS:
				result = SLD_ELEM_MARK_ATTR_WELLKNOWNNAME_VAL_CROSS;
				break;
			case SQUARE:
				result = SLD_ELEM_MARK_ATTR_WELLKNOWNNAME_VAL_SQUARE;
				break;
			case STAR:
				result = SLD_ELEM_MARK_ATTR_WELLKNOWNNAME_VAL_STAR;
				break;
			case TRIANGLE:
				result = SLD_ELEM_MARK_ATTR_WELLKNOWNNAME_VAL_TRIANGLE;
				break;
			case X:
				result = SLD_ELEM_MARK_ATTR_WELLKNOWNNAME_VAL_X;
				break;
			}
			return result;
		}

	}

	public static enum SLD_ELEM_ROOT_ATTR_VERSION_VAL {
		V1_0_0;
		public static SLD_ELEM_ROOT_ATTR_VERSION_VAL parse(String version) {
			SLD_ELEM_ROOT_ATTR_VERSION_VAL result = null;
			if (version.equals(SLD_ELEM_ROOT_ATTR_VERSION_VAL_V1_0_0))
				result = V1_0_0;
			return result;
		}

		public String toString() {
			String result = "";
			switch (this) {
			case V1_0_0:
				result = SLD_ELEM_ROOT_ATTR_VERSION_VAL_V1_0_0;
				break;
			}
			return result;
		}
	}

	public static final String CSS_PARAM_FILL_NAME = "fill";
	public static final String CSS_PARAM_STROKE_NAME = "stroke";
	public static final String CSS_PARAM_STROKEOPACITY_NAME = "stroke-opacity";
	public static final String CSS_PARAM_STROKEWIDTH_NAME = "stroke-width";
	public static final String CSS_PARAM_FILLOPACITY_NAME = "fill-opacity";
	public static final String CSS_PARAM_OPACITY_NAME = "opacity";

	public static final String CSS_PARAM_FONTSTYLE_VAL_ITALIC = "italic";

	public static final String CSS_PARAM_FONTSTYLE_VAL_NORMAL = "normal";
	public static final String CSS_PARAM_FONTWEIGHT_VAL_BOLD = "bold";
	public static final String CSS_PARAM_FONTWEIGHT_VAL_NORMAL = "normal";
	public static final String CSS_PARAM_FONTFAMILY = "font-family";
	public static final String CSS_PARAM_FONTSTYLE = "font-style";
	public static final String CSS_PARAM_FONTWEIGHT = "font-weight";
	public static final String CSS_PARAM_FONTSIZE = "font-size";

	public static final String FALSE = "FALSE";
	public static final String OGC_ELEM_ANY_ELEM_LITERAL_NAME = "ogc:Literal";
	public static final String OGC_ELEM_ANY_ELEM_PROPERTYNAME_NAME = "ogc:PropertyName";
	public static final String SLD_ELEM_ANY_ELEM_ABSTRACT_NAME = "se:Abstract";
	public static final String SLD_ELEM_ANY_ELEM_COLOR_NAME = "se:Color";
	public static final String SLD_ELEM_ANY_ELEM_WIDTH_NAME = "se:Width";
	public static final String SLD_ELEM_ANY_ELEM_DESC_NAME = "se:Description";
	public static final String SLD_ELEM_ANY_ELEM_NAME_NAME = "se:Name";
	public static final String SLD_ELEM_ANY_ELEM_TITLE_NAME = "se:Title";
	public static final String SLD_ELEM_CSSPARAMETER_ATTR_NAME_NAME = "name";
	public static final String SLD_ELEM_CSSPARAMETER_NAME = "se:CssParameter";
	public static final String SLD_ELEM_EXTERNALGRAPHIC_ELEM_FORMAT_NAME = "se:Format";

	public static final String SLD_ELEM_EXTERNALGRAPHIC_NAME = "se:ExternalGraphic";
	public static final String SLD_ELEM_FEATURETYPESTYLE_NAME = "se:FeatureTypeStyle";

	public static final String SLD_ELEM_FILL_NAME = "se:Fill";
	public static final String SLD_ELEM_HALO_NAME = "se:Halo";
	public static final String SLD_ELEM_FONT_NAME = "se:Font";
	public static final String SLD_ELEM_ANY_ELEM_OPACITY_NAME = "se:Opacity";
	public static final String SLD_ELEM_GRAPHIC_ELEM_ROTATION_NAME = "se:Rotation";

	public static final String SLD_ELEM_HALO_ELEM_RADIUS_NAME = "se:Radius";
	public static final String SLD_ELEM_GRAPHIC_ELEM_SIZE_NAME = "se:Size";
	public static final String SLD_ELEM_GRAPHIC_NAME = "se:Graphic";
	public static final String SLD_ELEM_GRAPHICFILL_NAME = "se:GraphicFill";

	public static final String SLD_ELEM_GRAPHICSTROKE_NAME = "se:GraphicStroke";
	public static final String SLD_ELEM_LEGENDGRAPHIC_NAME = "se:LegendGraphic";
	public static final String SLD_ELEM_LINESYMBOLIZER_NAME = "se:LineSymbolizer";
	public static final String SLD_ELEM_MARK_ATTR_WELLKNOWNNAME_VAL_CIRCLE = "circle";
	public static final String SLD_ELEM_MARK_ATTR_WELLKNOWNNAME_VAL_CROSS = "cross";
	public static final String SLD_ELEM_MARK_ATTR_WELLKNOWNNAME_VAL_SQUARE = "square";
	public static final String SLD_ELEM_MARK_ATTR_WELLKNOWNNAME_VAL_STAR = "star";
	public static final String SLD_ELEM_MARK_ATTR_WELLKNOWNNAME_VAL_TRIANGLE = "triangle";
	public static final String SLD_ELEM_MARK_ATTR_WELLKNOWNNAME_VAL_X = "x";
	public static final String SLD_ELEM_MARK_NAME = "se:Mark";
	public static final String SLD_ELEM_NAMEDLAYER_NAME = "se:NamedLayer";
	public static final String SLD_ELEM_NAMEDSTYLE_NAME = "se:NamedStyle";
	public static final String SLD_ELEM_ONLINERESOURCE_ATTR_HREF_NAME = "href";
	public static final String SLD_ELEM_ONLINERESOURCE_ATTR_TYPE_NAME = "type";
	public static final String SLD_ELEM_ONLINERESOURCE_ATTR_TYPE_VAL_SIMPLE = "simple";
	public static final String SLD_ELEM_ONLINERESOURCE_NAME = "se:OnlineResource";
	public static final String SLD_ELEM_POINTSYMBOLIZER_NAME = "se:PointSymbolizer";
	public static final String SLD_ELEM_POLYGONSYMBOLIZER_NAME = "se:PolygonSymbolizer";
	public static final String SLD_ELEM_ROOT_ATTR_VERSION_NAME = "version";
	public static final String SLD_ELEM_ROOT_ATTR_VERSION_VAL_V1_0_0 = "1.0.0";
	public static final String SLD_ELEM_ROOT_NAME = "se:StyledLayerDescriptor";
	public static final String SLD_ELEM_RULE_ELEM_MAXSCALEDENOM_NAME = "se:MaxScaleDenominator";
	public static final String SLD_ELEM_RULE_ELEM_MINSCALEDENOM_NAME = "se:MinScaleDenominator";
	public static final String SLD_ELEM_RULE_NAME = "se:Rule";
	public static final String SLD_ELEM_STROKE_ELEM_DASHARRAY_NAME = "DashArray";

	public static final String SLD_ELEM_STROKE_NAME = "se:Stroke";

	public static final String SLD_ELEM_TEXTSYMBOLIZER_NAME = "se:TextSymbolizer";
	public static final String SLD_ELEM_TEXTSYMBOLIZER_ELEM_LABEL_NAME = "se:Label";
	public static final String SLD_ELEM_USERSTYLE_ELEM_ISDEFAULT_NAME = "se:isDefault";
	public static final String SLD_ELEM_USERSTYLE_NAME = "se:UserStyle";
	public static final String SLD_ELEM_MARK_ELEM_WELLKNOWNNAME_NAME = "se:WellKnownName";

	public static final String XML_ELEM_ANY_ATTR_XSI_SCHEMA_LOC_VAL_SLD = "http://www.opengis.net/sld http://schemas.opengis.net/sld/1.0.0/StyledLayerDescriptor.xsd";
	public static final String XML_NS_PFIX_OPENGIS_NET_OGC = "ogc";

	public static final String XML_NS_PFIX_OPENGIS_NET_SLD = "sld";
	public static final String XML_NS_PFIX_W3_ORG_01_XSI = "xsi";
	public static final String XML_NS_PFIX_W3_ORG_99_XLINK = "xlink";

	public static final String XML_NS_URI_OPENGIS_NET_OGC = "http://www.opengis.net/ogc";
	public static final String XML_NS_URI_OPENGIS_NET_SLD = "http://www.opengis.net/sld";
	public static final String XML_NS_URI_W3_ORG_01_XSI = "http://www.w3.org/2001/XMLSchema-instance";
	public static final String XML_NS_URI_W3_ORG_99_XLINK = "http://www.w3.org/1999/xlink";

	public static final String XML_ELEM_ANY_ATTR_XMLNS_NAME = "xmlns";
	public static final String XML_ELEM_ANY_ATTR_XSI_SCHEMA_LOC_NAME = XML_NS_PFIX_W3_ORG_01_XSI
			+ COLON + "schemaLocation";

}
