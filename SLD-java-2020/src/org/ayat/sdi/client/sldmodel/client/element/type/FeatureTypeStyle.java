package org.ayat.sdi.client.sldmodel.client.element.type;

import org.ayat.sdi.client.sldmodel.client.StringPool;
import org.ayat.sdi.client.sldmodel.client.element.SLDElement;
import org.ayat.sdi.client.sldmodel.client.element.aspect.SLDElementHasRuleList;
import org.ayat.sdi.client.sldmodel.client.util.ElementNotFoundException;

public interface FeatureTypeStyle extends SLDElement, SLDElementHasRuleList {
	String TAG_NAME = StringPool.SLD_ELEM_FEATURETYPESTYLE_NAME;

	/**
	 * @throws ElementNotFoundException
	 */
	String getName() throws ElementNotFoundException;

	void setName(String name);

	/**
	 * @throws ElementNotFoundException
	 */
	String getTitle() throws ElementNotFoundException;

	void setTitle(String title);

	/**
	 * @throws ElementNotFoundException
	 */
	String getAbstract() throws ElementNotFoundException;

	void setAbstract(String abstractText);
}