package org.ayat.sdi.client.sldmodel.client.element.type.impl;

import org.ayat.sdi.client.sldmodel.client.StringPool;
import org.ayat.sdi.client.sldmodel.client.element.AbstractSLDElement;
import org.ayat.sdi.client.sldmodel.client.element.SLDElementFactory;
import org.ayat.sdi.client.sldmodel.client.element.SLDElementList;
import org.ayat.sdi.client.sldmodel.client.element.type.FeatureTypeStyle;
import org.ayat.sdi.client.sldmodel.client.element.type.Rule;
import org.ayat.sdi.client.sldmodel.client.util.ElementNotFoundException;
import org.ayat.sdi.client.sldmodel.client.util.UnknownElementTypeException;

import org.w3c.dom.*;

public class FeatureTypeStyleImpl extends AbstractSLDElement implements
		FeatureTypeStyle {

	public FeatureTypeStyleImpl(Document document) {
		super(document, FeatureTypeStyle.TAG_NAME);
	}

	public FeatureTypeStyleImpl(Element element) {
		super(element);
	}

	@Override
	public void setName(String name) {
		setTextGrandChildValue(StringPool.SLD_ELEM_ANY_ELEM_NAME_NAME, name);
	}

	@Override
	public String getName() throws ElementNotFoundException {
		return getTextGrandChildValue(StringPool.SLD_ELEM_ANY_ELEM_NAME_NAME);
	}

	@Override
	public void setTitle(String title) {
		setTextGrandChildValue(StringPool.SLD_ELEM_ANY_ELEM_TITLE_NAME, title);
	}

	@Override
	public String getTitle() throws ElementNotFoundException {
		return getTextGrandChildValue(StringPool.SLD_ELEM_ANY_ELEM_TITLE_NAME);
	}

	@Override
	public void setAbstract(String abstractText) {
		setTextGrandChildValue(StringPool.SLD_ELEM_ANY_ELEM_ABSTRACT_NAME,
				abstractText);
	}

	@Override
	public String getAbstract() throws ElementNotFoundException {
		return getTextGrandChildValue(StringPool.SLD_ELEM_ANY_ELEM_ABSTRACT_NAME);
	}

	@Override
	public void addRule(Rule rule) {
		element.appendChild(rule.getElement());
	}

	@Override
	public void removeRule(Rule rule) {
		element.removeChild(rule.getElement());
	}

	@Override
	public SLDElementList<Rule> getRuleList() {
		return new SLDElementList<Rule>(element
				.getElementsByTagName(Rule.TAG_NAME), Rule.class);
	}

	@Override
	public Rule createRule() {
		try {
			Rule result = SLDElementFactory.getInstance(Rule.class, document);
			addRule(result);
			return result;
		} catch (UnknownElementTypeException e) {// Will never happen
			throw new RuntimeException(e);
		}
	}
}
