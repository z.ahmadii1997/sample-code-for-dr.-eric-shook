package org.ayat.sdi.client.sldmodel.client.element.type;

import org.ayat.sdi.client.sldmodel.client.StringPool;
import org.ayat.sdi.client.sldmodel.client.StringPool.SLD_ELEM_MARK_ATTR_WELLKNOWNNAME_VAL;
import org.ayat.sdi.client.sldmodel.client.element.aspect.SLDElementHasFill;
import org.ayat.sdi.client.sldmodel.client.element.aspect.SLDElementHasStroke;
import org.ayat.sdi.client.sldmodel.client.util.ElementNotFoundException;

public interface Mark extends SLDElementHasStroke, SLDElementHasFill {
	String TAG_NAME = StringPool.SLD_ELEM_MARK_NAME;

	/**
	 * @throws ElementNotFoundException
	 */
	SLD_ELEM_MARK_ATTR_WELLKNOWNNAME_VAL getWellKnownName()
			throws ElementNotFoundException;

	void setWellKnownName(SLD_ELEM_MARK_ATTR_WELLKNOWNNAME_VAL value);
	void setWellKnownName(String value);
}
