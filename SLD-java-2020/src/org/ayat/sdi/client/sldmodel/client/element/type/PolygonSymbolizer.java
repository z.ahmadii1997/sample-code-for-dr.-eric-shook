package org.ayat.sdi.client.sldmodel.client.element.type;

import org.ayat.sdi.client.sldmodel.client.StringPool;
import org.ayat.sdi.client.sldmodel.client.element.aspect.SLDElementHasFill;
import org.ayat.sdi.client.sldmodel.client.element.aspect.SLDElementHasStroke;

public interface PolygonSymbolizer extends Symbolizer, SLDElementHasStroke, SLDElementHasFill {
	String TAG_NAME = StringPool.SLD_ELEM_POLYGONSYMBOLIZER_NAME;
}
