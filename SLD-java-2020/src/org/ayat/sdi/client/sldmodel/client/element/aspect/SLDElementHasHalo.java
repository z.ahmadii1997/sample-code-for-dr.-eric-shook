package org.ayat.sdi.client.sldmodel.client.element.aspect;

import org.ayat.sdi.client.sldmodel.client.element.SLDElement;
import org.ayat.sdi.client.sldmodel.client.element.type.Halo;
import org.ayat.sdi.client.sldmodel.client.util.ElementNotFoundException;

import org.w3c.dom.*;

public interface SLDElementHasHalo extends SLDElement {
	/**
	 * @throws ElementNotFoundException
	 */
	Halo getHalo() throws ElementNotFoundException;

	void setHalo(Halo value);

	/**
	 * Creates a {@link Halo} using the constructor {@link Halo}(
	 * {@link Document}) and passes it to {@link #setHalo(Halo)}.
	 * */
	Halo createHalo();

	/**
	 * If a {@link Halo} has been set before, removes it. Else does nothing.
	 * */
	void removeHalo();
}
