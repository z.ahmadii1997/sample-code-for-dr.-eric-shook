package org.ayat.sdi.client.sldmodel.client.element.aspect.impl;

import org.ayat.sdi.client.sldmodel.client.element.AbstractSLDElement;
import org.ayat.sdi.client.sldmodel.client.element.SLDElementFactory;
import org.ayat.sdi.client.sldmodel.client.element.aspect.SLDElementHasFill;
import org.ayat.sdi.client.sldmodel.client.element.type.Fill;
import org.ayat.sdi.client.sldmodel.client.util.ElementNotFoundException;
import org.ayat.sdi.client.sldmodel.client.util.UnknownElementTypeException;

import org.w3c.dom.*;

public abstract class SLDElementHasFillImpl extends AbstractSLDElement
		implements SLDElementHasFill {
	public SLDElementHasFillImpl(Document document, String tagName) {
		super(document, tagName);
	}

	public SLDElementHasFillImpl(Element element) {
		super(element);
	}

	@Override
	public void setFill(Fill value) {
		setUniqueChild(value.getElement());
	}

	@Override
	public Fill getFill() throws ElementNotFoundException {
		try {
			return SLDElementFactory.getInstance(Fill.class,
					getUniqueChild(Fill.TAG_NAME));
		} catch (Exception e) {
			throw new ElementNotFoundException(Fill.TAG_NAME,e);
		}
	}

	@Override
	public Fill createFill() {
		try {
			Fill result = SLDElementFactory.getInstance(Fill.class, document);
			setFill(result);
			return result;
		} catch (UnknownElementTypeException e) {// Will never happen
			throw new RuntimeException(e);
		}
	}

	@Override
	public void removeFill() {
		try {
			element.removeChild(getFill().getElement());
		} catch (ElementNotFoundException e) {
			// Do nothing. there is no fill to be removed.
		}
	}
}
