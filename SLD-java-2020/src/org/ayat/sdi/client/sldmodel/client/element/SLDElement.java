package org.ayat.sdi.client.sldmodel.client.element;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

public interface SLDElement {
	/**
	 * Hide this in every inheritance by redeclaring it. DO NOT ASIGN VALUE TO
	 * THIS FIELD WITH OUT REDECLARATION!
	 * */
	String TAG_NAME = "SLDElement";

	/**
	 * Returns the document to which the element wrapped in this node wrapper
	 * belongs.
	 */
	Document getDocument();

	/** Returns the element wrapped in this node wrapper. */
	Element getElement();

	/**
	 * Returns the name of the element wrapped in this SLDElement.<br/>
	 * e.g. If we are wrapping a &lt DUMMY &gt element, returns the tag name:
	 * "DUMMY"
	 */
	String getTagName();
}