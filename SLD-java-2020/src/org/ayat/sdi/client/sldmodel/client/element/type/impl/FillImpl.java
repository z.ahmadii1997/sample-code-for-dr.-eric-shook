package org.ayat.sdi.client.sldmodel.client.element.type.impl;

import org.ayat.sdi.client.sldmodel.client.StringPool;
import org.ayat.sdi.client.sldmodel.client.element.SLDElementFactory;
import org.ayat.sdi.client.sldmodel.client.element.aspect.impl.SLDElementHasCSSParamsImpl;
import org.ayat.sdi.client.sldmodel.client.element.type.Fill;
import org.ayat.sdi.client.sldmodel.client.element.type.GraphicFill;
import org.ayat.sdi.client.sldmodel.client.util.ElementNotFoundException;
import org.ayat.sdi.client.sldmodel.client.util.InvalidTextFatherException;
import org.ayat.sdi.client.sldmodel.client.util.UnknownElementTypeException;

import org.w3c.dom.*;

public class FillImpl extends SLDElementHasCSSParamsImpl implements Fill {
	public FillImpl(Document document) {
		super(document, Fill.TAG_NAME);
	}

	public FillImpl(Element element) {
		super(element);
	}

	@Override
	public String getFill() throws InvalidTextFatherException,
			ElementNotFoundException {
		return getUniqueCSSParameter(StringPool.CSS_PARAM_FILL_NAME)
				.getCSSRuleValue();
	}

	@Override
	public Double getFillOpacity() throws NumberFormatException,
			InvalidTextFatherException, ElementNotFoundException {
		return Double.parseDouble(getUniqueCSSParameter(
				StringPool.CSS_PARAM_FILLOPACITY_NAME).getCSSRuleValue());
	}

	@Override
	public GraphicFill getGraphicFill() throws ElementNotFoundException {
		try {
			return SLDElementFactory.getInstance(GraphicFill.class,
					getUniqueChild(GraphicFill.TAG_NAME));
		} catch (UnknownElementTypeException e) {// Will never happen
			throw new RuntimeException(e);
		}
	}

	@Override
	public void setFill(String rrggbb) {
		setUniqueOGCLiteral(new CSSParameterImpl(document,
				StringPool.CSS_PARAM_FILL_NAME), StringPool.SHARP + rrggbb);
	}

	@Override
	public void setFillOpacity(double opacity) {
		setUniqueOGCLiteral(new CSSParameterImpl(document,
				StringPool.CSS_PARAM_FILLOPACITY_NAME), Double
				.toString(opacity));
	}

	@Override
	public void setGraphicFill(GraphicFill value) {
		setUniqueChild(value.getElement());
	}

	@Override
	public GraphicFill createGraphicFill() {
		try {
			GraphicFill result = SLDElementFactory.getInstance(
					GraphicFill.class, document);
			setGraphicFill(result);
			return result;
		} catch (UnknownElementTypeException e) {// Will never happen
			throw new RuntimeException(e);
		}
	}

	@Override
	public void removeGraphicFill() {
		try {
			element.removeChild(getGraphicFill().getElement());
		} catch (ElementNotFoundException e) {
			// Nothing to remove.
		}
	}
}
