package org.ayat.sdi.client.sldmodel.client.element.type.impl;

import org.ayat.sdi.client.sldmodel.client.StringPool;
import org.ayat.sdi.client.sldmodel.client.StringPool.CSS_PARAM_FONTSTYLE_VAL;
import org.ayat.sdi.client.sldmodel.client.StringPool.CSS_PARAM_FONTWEIGHT_VAL;
import org.ayat.sdi.client.sldmodel.client.element.aspect.impl.SLDElementHasCSSParamsImpl;
import org.ayat.sdi.client.sldmodel.client.element.type.Font;
import org.ayat.sdi.client.sldmodel.client.util.ElementNotFoundException;
import org.ayat.sdi.client.sldmodel.client.util.InvalidTextFatherException;

import org.w3c.dom.*;

public class FontImpl extends SLDElementHasCSSParamsImpl implements Font {
	public FontImpl(Document document) {
		super(document, Font.TAG_NAME);
	}

	public FontImpl(Element element) {
		super(element);
	}

	@Override
	public String getFontFamily() throws InvalidTextFatherException,
			ElementNotFoundException {
		return getUniqueCSSParameter(StringPool.CSS_PARAM_FONTFAMILY)
				.getCSSRuleValue();
	}

	@Override
	public Integer getFontSize() throws NumberFormatException,
			InvalidTextFatherException, ElementNotFoundException {
		return Integer.parseInt(getUniqueCSSParameter(
				StringPool.CSS_PARAM_FONTSIZE).getCSSRuleValue());
	}

	@Override
	public CSS_PARAM_FONTSTYLE_VAL getFontStyle()
			throws InvalidTextFatherException, ElementNotFoundException {
		return CSS_PARAM_FONTSTYLE_VAL.parse(getUniqueCSSParameter(
				StringPool.CSS_PARAM_FONTSTYLE).getCSSRuleValue());
	}

	@Override
	public CSS_PARAM_FONTWEIGHT_VAL getFontWeight()
			throws InvalidTextFatherException, ElementNotFoundException {
		return CSS_PARAM_FONTWEIGHT_VAL.parse(getUniqueCSSParameter(
				StringPool.CSS_PARAM_FONTWEIGHT).getCSSRuleValue());
	}

	@Override
	public void setFontFamily(String value) {
		setTextGrandChildValue(StringPool.CSS_PARAM_FONTFAMILY, value);
	}

	@Override
	public void setFontSize(int value) {
		setIntegerGrandChildValue(StringPool.CSS_PARAM_FONTSIZE, value);
	}

	@Override
	public void setFontStyle(CSS_PARAM_FONTSTYLE_VAL value) {
		setTextGrandChildValue(StringPool.CSS_PARAM_FONTSTYLE, value.toString());
	}

	@Override
	public void setFontWeight(CSS_PARAM_FONTWEIGHT_VAL value) {
		setTextGrandChildValue(StringPool.CSS_PARAM_FONTWEIGHT, value
				.toString());
	}
}
