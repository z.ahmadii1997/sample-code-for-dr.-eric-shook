package org.ayat.sdi.client.sldmodel.client.element.type;

import org.ayat.sdi.client.sldmodel.client.StringPool;
import org.ayat.sdi.client.sldmodel.client.element.SLDElement;
import org.ayat.sdi.client.sldmodel.client.element.aspect.SLDElementHasOnlineResource;
import org.ayat.sdi.client.sldmodel.client.util.ElementNotFoundException;
import  org.ayat.sdi.client.sldmodel.client.core.StringPool.MIME_TYPE;

public interface ExternalGraphic extends SLDElement,
		SLDElementHasOnlineResource {
	String TAG_NAME = StringPool.SLD_ELEM_EXTERNALGRAPHIC_NAME;

	/**
	 * @throws ElementNotFoundException
	 */
	MIME_TYPE getFormat() throws ElementNotFoundException;

	void setFormat(MIME_TYPE value);
}
