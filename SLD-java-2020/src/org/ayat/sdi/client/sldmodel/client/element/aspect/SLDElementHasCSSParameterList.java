package org.ayat.sdi.client.sldmodel.client.element.aspect;

import org.ayat.sdi.client.sldmodel.client.element.SLDElement;
import org.ayat.sdi.client.sldmodel.client.element.SLDElementCollection;
import org.ayat.sdi.client.sldmodel.client.element.type.CSSParameter;
import org.ayat.sdi.client.sldmodel.client.util.ElementNotFoundException;

import org.w3c.dom.*;

public interface SLDElementHasCSSParameterList extends SLDElement {
	void addCSSParameter(CSSParameter value);

	CSSParameter getCSSParameter(int index);

	/**
	 * Drops all previous {@link CSSParameter}s with the same rule name and
	 * appends the new one.
	 * */
	void setUniqueCSSParameter(CSSParameter value);

	/**
	 * Use of this method is essentially meaningless unless
	 * {@link #setUniqueCSSParameter(CSSParameter)} has been used to store the
	 * expected value.<br/>
	 * Returns null if no such {@link CSSParameter} value has been set before.
	 * 
	 * @throws ElementNotFoundException
	 * */
	CSSParameter getUniqueCSSParameter(String cssRuleName)
			throws ElementNotFoundException;

	void removeCSSParameter(CSSParameter value);

	SLDElementCollection<CSSParameter> getCSSParameters();

	/**
	 * Creates a {@link CSSParameter} using the constructor {@link CSSParameter}
	 * ({@link Document}) and passes it to
	 * {@link #addCSSParameter(CSSParameter)}.
	 * */
	CSSParameter createCSSParameter();

	void setUniqueOGCLiteral(CSSParameter value, String literal);
}