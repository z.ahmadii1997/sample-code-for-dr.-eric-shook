package org.ayat.sdi.client.sldmodel.client.core;

public class NameValuePair {
	String name;
	String value;
	public NameValuePair(String name, String value) {
		super();
		this.name = name;
		this.value = value;
	}
	public NameValuePair(String nameValuePair) {
		String[] parts=nameValuePair.split(StringPool.EQUAL_SIGN);
		if(parts.length!=2)
			throw new RuntimeException("Invalid name value pair: '"+nameValuePair+"'");
		name=parts[0];
		value=parts[1];
	}
	public String getName() {
		return name;
	}
	public String getValue() {
		return value;
	}
	@Override
	public String toString() {
		return name+StringPool.EQUAL_SIGN+value;
	}
}
