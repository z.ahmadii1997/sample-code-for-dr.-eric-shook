package org.ayat.sdi.client.sldmodel.client.util;

import java.util.ArrayList;

public interface SimpleList<Type> {
	Type get(int index);

	ArrayList<Type> get();

	void add(Type value);

	void remove(int index);

	void clear();
}
