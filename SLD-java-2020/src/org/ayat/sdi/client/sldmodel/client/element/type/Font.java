package org.ayat.sdi.client.sldmodel.client.element.type;

import org.ayat.sdi.client.sldmodel.client.StringPool;
import org.ayat.sdi.client.sldmodel.client.StringPool.CSS_PARAM_FONTSTYLE_VAL;
import org.ayat.sdi.client.sldmodel.client.StringPool.CSS_PARAM_FONTWEIGHT_VAL;
import org.ayat.sdi.client.sldmodel.client.element.aspect.SLDElementHasCSSParameterList;
import org.ayat.sdi.client.sldmodel.client.util.ElementNotFoundException;
import org.ayat.sdi.client.sldmodel.client.util.InvalidTextFatherException;

public interface Font extends SLDElementHasCSSParameterList {
	String TAG_NAME = StringPool.SLD_ELEM_FONT_NAME;

	/**
	 * @throws ElementNotFoundException
	 * @throws InvalidTextFatherException
	 */
	String getFontFamily() throws InvalidTextFatherException,
			ElementNotFoundException;

	void setFontFamily(String value);

	/**
	 * @throws ElementNotFoundException
	 * @throws InvalidTextFatherException
	 * @throws NumberFormatException
	 */
	Integer getFontSize() throws NumberFormatException,
			InvalidTextFatherException, ElementNotFoundException;

	void setFontSize(int value);

	/**
	 * @throws ElementNotFoundException
	 * @throws InvalidTextFatherException
	 */
	CSS_PARAM_FONTSTYLE_VAL getFontStyle() throws InvalidTextFatherException,
			ElementNotFoundException;

	void setFontStyle(CSS_PARAM_FONTSTYLE_VAL value);

	/**
	 * @throws ElementNotFoundException
	 * @throws InvalidTextFatherException
	 */
	CSS_PARAM_FONTWEIGHT_VAL getFontWeight() throws InvalidTextFatherException,
			ElementNotFoundException;

	void setFontWeight(CSS_PARAM_FONTWEIGHT_VAL value);
}
