package org.ayat.sdi.client.sldmodel.client.element.aspect;

import org.ayat.sdi.client.sldmodel.client.element.SLDElement;
import org.ayat.sdi.client.sldmodel.client.element.type.GraphicStroke;
import org.ayat.sdi.client.sldmodel.client.util.ElementNotFoundException;

import org.w3c.dom.*;

public interface SLDElementHasGraphicStroke extends SLDElement {
	/**
	 * @throws ElementNotFoundException
	 */
	GraphicStroke getGraphicStroke() throws ElementNotFoundException;

	void setGraphicStroke(GraphicStroke value);

	/**
	 * If a {@link GraphicStroke} has been set before, removes it. Else does
	 * nothing.
	 * */
	void removeGraphicStroke();

	/**
	 * Creates a {@link GraphicStroke} using the constructor
	 * {@link GraphicStroke}({@link Document}) and passes it to
	 * {@link #setGraphicStroke(GraphicStroke)}.
	 * */
	GraphicStroke createGraphicStroke();

}
