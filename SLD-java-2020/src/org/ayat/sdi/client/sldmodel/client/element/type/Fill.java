package org.ayat.sdi.client.sldmodel.client.element.type;

import org.ayat.sdi.client.sldmodel.client.StringPool;
import org.ayat.sdi.client.sldmodel.client.element.aspect.SLDElementHasCSSParameterList;
import org.ayat.sdi.client.sldmodel.client.element.aspect.SLDElementHasGraphicFill;
import org.ayat.sdi.client.sldmodel.client.util.ElementNotFoundException;
import org.ayat.sdi.client.sldmodel.client.util.InvalidTextFatherException;

public interface Fill extends SLDElementHasCSSParameterList,
		SLDElementHasGraphicFill {
	String TAG_NAME = StringPool.SLD_ELEM_FILL_NAME;

	/** Sets a CSS parameter denoting a solid fill. */
	void setFill(String rrggbb);

	/**
	 * @throws ElementNotFoundException
	 * @throws InvalidTextFatherException
	 */
	String getFill() throws InvalidTextFatherException,
			ElementNotFoundException;

	/**
	 * Sets a CSS parameter denoting fill opacity.
	 * 
	 * @throws ElementNotFoundException
	 * @throws InvalidTextFatherException
	 * @throws NumberFormatException
	 */
	Double getFillOpacity() throws NumberFormatException,
			InvalidTextFatherException, ElementNotFoundException;

	void setFillOpacity(double opacity);
}
