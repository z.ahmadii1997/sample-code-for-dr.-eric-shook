package org.ayat.sdi.client.sldmodel.client.element.aspect;

import org.ayat.sdi.client.sldmodel.client.element.SLDElement;
import org.ayat.sdi.client.sldmodel.client.element.SLDElementList;
import org.ayat.sdi.client.sldmodel.client.element.type.NamedLayer;

import org.w3c.dom.*;

public interface SLDElementHasNamedLayerList extends SLDElement {
	SLDElementList<NamedLayer> getNamedLayers();

	void addNamedLayer(NamedLayer namedLayer);

	/**
	 * If a {@link NamedLayer} has been set before, removes it. Else does
	 * nothing.
	 * */
	void removeNamedLayer(NamedLayer namedLayer);

	/**
	 * Creates a {@link NamedLayer} using the constructor {@link NamedLayer} (
	 * {@link Document}) and passes it to {@link #addNamedLayer(NamedLayer)}.
	 * */
	NamedLayer createNamedLayer();
}
