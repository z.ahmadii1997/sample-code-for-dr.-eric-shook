package org.ayat.sdi.client.sldmodel.client.element.type.impl;

import org.ayat.sdi.client.sldmodel.client.element.aspect.impl.SLDElementHasCSSParamsHasGraphicImpl;
import org.ayat.sdi.client.sldmodel.client.element.type.GraphicStroke;

import org.w3c.dom.*;

public class GraphicStrokeImpl extends
		SLDElementHasCSSParamsHasGraphicImpl implements GraphicStroke {
	public GraphicStrokeImpl(Document document) {
		super(document, GraphicStroke.TAG_NAME);
	}

	public GraphicStrokeImpl(Element element) {
		super(element);
	}
}
