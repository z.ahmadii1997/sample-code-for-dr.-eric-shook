package org.ayat.sdi.client.sldmodel.client.element.type;

import org.ayat.sdi.client.sldmodel.client.StringPool;
import org.ayat.sdi.client.sldmodel.client.element.aspect.SLDElementHasFeatureTypeStyleList;
import org.ayat.sdi.client.sldmodel.client.element.aspect.SLDElementHasSelfDescription;
import org.ayat.sdi.client.sldmodel.client.util.ElementNotFoundException;

public interface UserStyle extends SLDElementHasSelfDescription,
		SLDElementHasFeatureTypeStyleList {
	String TAG_NAME = StringPool.SLD_ELEM_USERSTYLE_NAME;

	/**
	 * @throws ElementNotFoundException
	 */
	Boolean isDefault() throws ElementNotFoundException;

	void setDefault(boolean value);
}