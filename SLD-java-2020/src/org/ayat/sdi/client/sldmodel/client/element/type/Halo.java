package org.ayat.sdi.client.sldmodel.client.element.type;

import org.ayat.sdi.client.sldmodel.client.StringPool;
import org.ayat.sdi.client.sldmodel.client.element.aspect.SLDElementHasFill;
import org.ayat.sdi.client.sldmodel.client.util.ElementNotFoundException;

public interface Halo extends SLDElementHasFill {
	String TAG_NAME = StringPool.SLD_ELEM_HALO_NAME;

	/**
	 * Returns the radius of the halo around label.
	 * @throws ElementNotFoundException 
	 * @throws NumberFormatException 
	 * */
	int getRadius() throws NumberFormatException, ElementNotFoundException;

	/**
	 * Sets the radius of the halo around label.
	 * */
	void setRadius(int value);
}
