package org.ayat.sdi.client.sldmodel.client.element.type.impl;

import org.ayat.sdi.client.sldmodel.client.StringPool;
import org.ayat.sdi.client.sldmodel.client.element.AbstractSLDElement;
import org.ayat.sdi.client.sldmodel.client.element.type.CSSParameter;
import org.ayat.sdi.client.sldmodel.client.util.InvalidTextFatherException;

import org.w3c.dom.*;

public class CSSParameterImpl extends AbstractSLDElement implements
		CSSParameter {

	public CSSParameterImpl(Document document) {
		super(document, CSSParameter.TAG_NAME);
	}

	public CSSParameterImpl(Element element) {
		super(element);
	}

	CSSParameterImpl(Document document, String cssRuleName, String cssRuleValue) {
		this(document);
		setCSSRuleName(cssRuleName);
		setCSSRuleValue(cssRuleValue);
	}
	
	CSSParameterImpl(Document document, String cssRuleName) {
		this(document);
		setCSSRuleName(cssRuleName);
	}

	@Override
	public String getCSSRuleName() {
		return element
				.getAttribute(StringPool.SLD_ELEM_CSSPARAMETER_ATTR_NAME_NAME);
	}

	@Override
	public String getCSSRuleValue() throws InvalidTextFatherException {
		return getTextValue();
	}

	@Override
	public void setCSSRuleName(String value) {
		element.setAttribute(StringPool.SLD_ELEM_CSSPARAMETER_ATTR_NAME_NAME,
				value);
	}

	@Override
	public void setCSSRuleValue(String value) {
		setTextValue(value);
	}
}
