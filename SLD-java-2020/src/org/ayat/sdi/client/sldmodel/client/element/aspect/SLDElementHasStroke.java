package org.ayat.sdi.client.sldmodel.client.element.aspect;

import org.ayat.sdi.client.sldmodel.client.element.SLDElement;
import org.ayat.sdi.client.sldmodel.client.element.type.Stroke;
import org.ayat.sdi.client.sldmodel.client.util.ElementNotFoundException;

import org.w3c.dom.*;

public interface SLDElementHasStroke extends SLDElement {
	/**
	 * @throws ElementNotFoundException
	 */
	Stroke getStroke() throws ElementNotFoundException;

	void setStroke(Stroke value);

	/**
	 * Creates a {@link Stroke} using the constructor {@link Stroke}(
	 * {@link Document}) and passes it to {@link #setStroke(Stroke)}.
	 * */
	Stroke createStroke();

	/**
	 * If a {@link Stroke} has been set before, removes it. Else does nothing.
	 * */
	void removeStroke();
}
