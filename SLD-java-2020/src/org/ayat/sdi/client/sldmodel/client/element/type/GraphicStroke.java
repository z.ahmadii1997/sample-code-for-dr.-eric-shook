package org.ayat.sdi.client.sldmodel.client.element.type;

import org.ayat.sdi.client.sldmodel.client.StringPool;
import org.ayat.sdi.client.sldmodel.client.element.aspect.SLDElementHasCSSParameterList;
import org.ayat.sdi.client.sldmodel.client.element.aspect.SLDElementHasGraphic;

public interface GraphicStroke extends SLDElementHasCSSParameterList,
		SLDElementHasGraphic {
	String TAG_NAME = StringPool.SLD_ELEM_GRAPHICSTROKE_NAME;
}
