package org.ayat.sdi.client.sldmodel.client.element.aspect;

import org.ayat.sdi.client.sldmodel.client.element.SLDElement;
import org.ayat.sdi.client.sldmodel.client.element.type.UserStyle;
import org.ayat.sdi.client.sldmodel.client.util.ElementNotFoundException;

import org.w3c.dom.*;

public interface SLDElementHasUserStyle extends SLDElement {
	void setUserStyle(UserStyle userStyle);

	/**
	 * If a {@link UserStyle} has been set before, removes it. Else does
	 * nothing.
	 * */
	void removeUserStyle();

	UserStyle getUserStyle() throws ElementNotFoundException;

	/**
	 * Creates a {@link UserStyle} using the constructor {@link UserStyle}
	 * ({@link Document}) and passes it to {@link #setUserStyle(UserStyle)}.
	 * */
	UserStyle createUserStyle();
}
