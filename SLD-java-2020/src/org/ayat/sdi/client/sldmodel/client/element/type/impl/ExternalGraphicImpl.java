package org.ayat.sdi.client.sldmodel.client.element.type.impl;

import org.ayat.sdi.client.sldmodel.client.StringPool;
import org.ayat.sdi.client.sldmodel.client.element.AbstractSLDElement;
import org.ayat.sdi.client.sldmodel.client.element.SLDElementFactory;
import org.ayat.sdi.client.sldmodel.client.element.type.ExternalGraphic;
import org.ayat.sdi.client.sldmodel.client.element.type.OnlineResource;
import org.ayat.sdi.client.sldmodel.client.util.ElementNotFoundException;
import org.ayat.sdi.client.sldmodel.client.util.UnknownElementTypeException;
import static org.ayat.sdi.client.sldmodel.client.core.StringPool.MIME_TYPE;

import org.w3c.dom.*;

public class ExternalGraphicImpl extends AbstractSLDElement implements
		ExternalGraphic {
	public ExternalGraphicImpl(Document document) {
		super(document, ExternalGraphic.TAG_NAME);
	}

	public ExternalGraphicImpl(Element element) {
		super(element);
	}

	@Override
	public MIME_TYPE getFormat() throws ElementNotFoundException {
		return MIME_TYPE
				.parse(getTextGrandChildValue(StringPool.SLD_ELEM_EXTERNALGRAPHIC_ELEM_FORMAT_NAME));
	}

	@Override
	public void setFormat(MIME_TYPE value) {
		setTextGrandChildValue(
				StringPool.SLD_ELEM_EXTERNALGRAPHIC_ELEM_FORMAT_NAME, value
						.toMIMEString());
	}

	@Override
	public void setOnlineResource(OnlineResource value) {
		setUniqueChild(value.getElement());
	}

	@Override
	public OnlineResource getOnlineResource() throws ElementNotFoundException {
		try {
			return SLDElementFactory.getInstance(OnlineResource.class,
					getUniqueChild(StringPool.SLD_ELEM_ONLINERESOURCE_NAME));
		} catch (UnknownElementTypeException e) {// Will never happen
			throw new RuntimeException(e);
		}
	}

	@Override
	public OnlineResource createOnlineResource() {
		try {
			OnlineResource result = SLDElementFactory.getInstance(
					OnlineResource.class, document);
			setOnlineResource(result);
			return result;
		} catch (UnknownElementTypeException e) {// Will never happen
			throw new RuntimeException(e);
		}
	}

	@Override
	public void removeOnlineResource() {
		try {
			element.removeChild(getOnlineResource().getElement());
		} catch (ElementNotFoundException e) {
			//Nothing to remove.
		}
	}

}
