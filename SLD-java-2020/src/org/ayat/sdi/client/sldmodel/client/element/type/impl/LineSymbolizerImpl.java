package org.ayat.sdi.client.sldmodel.client.element.type.impl;

import org.ayat.sdi.client.sldmodel.client.element.aspect.impl.SLDElementHasStrokeImpl;
import org.ayat.sdi.client.sldmodel.client.element.type.LineSymbolizer;

import org.w3c.dom.*;

public class LineSymbolizerImpl extends SLDElementHasStrokeImpl implements
		LineSymbolizer {
	public LineSymbolizerImpl(Document document) {
		super(document, LineSymbolizer.TAG_NAME);
	}

	public LineSymbolizerImpl(Element element) {
		super(element);
	}
}
