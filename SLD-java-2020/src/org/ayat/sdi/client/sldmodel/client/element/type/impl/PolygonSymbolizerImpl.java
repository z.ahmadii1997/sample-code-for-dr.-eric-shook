package org.ayat.sdi.client.sldmodel.client.element.type.impl;

import org.ayat.sdi.client.sldmodel.client.element.aspect.impl.SLDElementHasFillHasStrokeImpl;
import org.ayat.sdi.client.sldmodel.client.element.type.PolygonSymbolizer;

import org.w3c.dom.*;

public class PolygonSymbolizerImpl extends SLDElementHasFillHasStrokeImpl
		implements PolygonSymbolizer {
	public PolygonSymbolizerImpl(Document document) {
		super(document, PolygonSymbolizer.TAG_NAME);
	}

	public PolygonSymbolizerImpl(Element element) {
		super(element);
	}
}
