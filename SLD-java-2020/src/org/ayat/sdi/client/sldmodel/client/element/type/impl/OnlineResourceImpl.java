package org.ayat.sdi.client.sldmodel.client.element.type.impl;

import org.ayat.sdi.client.sldmodel.client.StringPool;
import org.ayat.sdi.client.sldmodel.client.element.AbstractSLDElement;
import org.ayat.sdi.client.sldmodel.client.element.type.OnlineResource;

import org.w3c.dom.*;

public class OnlineResourceImpl extends AbstractSLDElement implements
		OnlineResource {
	public OnlineResourceImpl(Document document) {
		super(document, OnlineResource.TAG_NAME);
	}

	public OnlineResourceImpl(Element element) {
		super(element);
	}

	@Override
	protected void doInitElement() {
		element.setAttribute(StringPool.XML_ELEM_ANY_ATTR_XMLNS_NAME
				+ StringPool.COLON + StringPool.XML_NS_PFIX_W3_ORG_99_XLINK,
				StringPool.XML_NS_URI_W3_ORG_99_XLINK);
		element.setAttribute(StringPool.XML_NS_PFIX_W3_ORG_99_XLINK
				+ StringPool.COLON
				+ StringPool.SLD_ELEM_ONLINERESOURCE_ATTR_TYPE_NAME,
				StringPool.SLD_ELEM_ONLINERESOURCE_ATTR_TYPE_VAL_SIMPLE);
	}

	@Override
	public String getHREF() {
		return element.getAttribute(StringPool.XML_NS_PFIX_W3_ORG_99_XLINK
				+ StringPool.COLON
				+ StringPool.SLD_ELEM_ONLINERESOURCE_ATTR_HREF_NAME);
	}

	@Override
	public void setHREF(String value) {
		element.setAttribute(StringPool.XML_NS_PFIX_W3_ORG_99_XLINK
				+ StringPool.COLON
				+ StringPool.SLD_ELEM_ONLINERESOURCE_ATTR_HREF_NAME, value);
	}
}
