package org.ayat.sdi.client.sldmodel.client.element.type;

import org.ayat.sdi.client.sldmodel.client.StringPool;
import org.ayat.sdi.client.sldmodel.client.element.aspect.SLDElementHasStroke;

public interface LineSymbolizer extends Symbolizer, SLDElementHasStroke {
	String TAG_NAME = StringPool.SLD_ELEM_LINESYMBOLIZER_NAME;
}
