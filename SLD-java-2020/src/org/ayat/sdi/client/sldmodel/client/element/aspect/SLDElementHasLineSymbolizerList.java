package org.ayat.sdi.client.sldmodel.client.element.aspect;

import org.ayat.sdi.client.sldmodel.client.element.SLDElement;
import org.ayat.sdi.client.sldmodel.client.element.SLDElementCollection;
import org.ayat.sdi.client.sldmodel.client.element.type.LineSymbolizer;

import org.w3c.dom.*;

public interface SLDElementHasLineSymbolizerList extends SLDElement {
	/**
	 * Adds a {@link LineSymbolizer}.
	 * */
	void addLineSymbolizer(LineSymbolizer value);

	/**
	 * Returns the {@link LineSymbolizer} at the specified index.
	 * */
	LineSymbolizer getLineSymbolizer(int index);

	/**
	 * Removes the specified {@link LineSymbolizer}.
	 * */
	void removeLineSymbolizer(LineSymbolizer value);

	/**
	 * Returns all {@link LineSymbolizer}s.
	 * */
	SLDElementCollection<LineSymbolizer> getLineSymbolizers();

	/**
	 * Removes all {@link LineSymbolizer}s.
	 * */
	void removeLineSymbolizers();

	/**
	 * Creates a {@link LineSymbolizer} using the constructor
	 * {@link LineSymbolizer}({@link Document}) and passes it to
	 * {@link #addLineSymbolizer(LineSymbolizer)}.
	 * */
	LineSymbolizer createLineSymbolizer();
}