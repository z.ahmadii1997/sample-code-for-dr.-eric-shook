package org.ayat.sdi.client.sldmodel.client.element.aspect;

import org.ayat.sdi.client.sldmodel.client.element.SLDElement;
import org.ayat.sdi.client.sldmodel.client.element.type.OnlineResource;
import org.ayat.sdi.client.sldmodel.client.util.ElementNotFoundException;

import org.w3c.dom.*;

public interface SLDElementHasOnlineResource extends SLDElement {
	/**
	 * @throws ElementNotFoundException
	 */
	OnlineResource getOnlineResource() throws ElementNotFoundException;

	void setOnlineResource(OnlineResource value);

	/**
	 * If an {@link OnlineResource} has been set before, removes it. Else does
	 * nothing.
	 * */
	void removeOnlineResource();

	/**
	 * Creates an {@link OnlineResource} using the constructor
	 * {@link OnlineResource}({@link Document}) and passes it to
	 * {@link #setOnlineResource(OnlineResource)}.
	 * */
	OnlineResource createOnlineResource();

}
