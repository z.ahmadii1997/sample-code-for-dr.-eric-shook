package org.ayat.sdi.client.sldmodel.client.element.type;

import org.ayat.sdi.client.sldmodel.client.StringPool;
import org.ayat.sdi.client.sldmodel.client.element.aspect.SLDElementHasGraphic;

public interface PointSymbolizer extends Symbolizer, SLDElementHasGraphic {
	String TAG_NAME = StringPool.SLD_ELEM_POINTSYMBOLIZER_NAME;
}
