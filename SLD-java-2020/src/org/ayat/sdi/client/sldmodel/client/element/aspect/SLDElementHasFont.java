package org.ayat.sdi.client.sldmodel.client.element.aspect;

import org.ayat.sdi.client.sldmodel.client.element.SLDElement;
import org.ayat.sdi.client.sldmodel.client.element.type.Font;
import org.ayat.sdi.client.sldmodel.client.util.ElementNotFoundException;

import org.w3c.dom.*;

public interface SLDElementHasFont extends SLDElement {
	/**
	 * @throws ElementNotFoundException
	 */
	Font getFont() throws ElementNotFoundException;

	void setFont(Font value);

	/**
	 * If a {@link Font} has been set before, removes it. Else does nothing.
	 * */
	void removeFont();

	/**
	 * Creates a {@link Font} using the constructor {@link Font}(
	 * {@link Document}) and passes it to {@link #setFont(Font)}.
	 * */
	Font createFont();
}
