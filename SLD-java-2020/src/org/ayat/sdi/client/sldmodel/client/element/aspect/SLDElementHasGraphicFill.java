package org.ayat.sdi.client.sldmodel.client.element.aspect;

import org.ayat.sdi.client.sldmodel.client.element.SLDElement;
import org.ayat.sdi.client.sldmodel.client.element.type.GraphicFill;
import org.ayat.sdi.client.sldmodel.client.util.ElementNotFoundException;

import org.w3c.dom.*;

public interface SLDElementHasGraphicFill extends SLDElement {
	/**
	 * @throws ElementNotFoundException
	 */
	GraphicFill getGraphicFill() throws ElementNotFoundException;

	void setGraphicFill(GraphicFill value);

	/**
	 * If a {@link GraphicFill} has been set before, removes it. Else does
	 * nothing.
	 * */
	void removeGraphicFill();

	/**
	 * Creates a {@link GraphicFill} using the constructor {@link GraphicFill} (
	 * {@link Document}) and passes it to {@link #setGraphicFill(GraphicFill)}.
	 * */
	GraphicFill createGraphicFill();
}
