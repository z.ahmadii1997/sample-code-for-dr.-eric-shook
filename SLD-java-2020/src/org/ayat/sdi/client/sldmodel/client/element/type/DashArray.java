package org.ayat.sdi.client.sldmodel.client.element.type;

import org.ayat.sdi.client.sldmodel.client.StringPool;
import org.ayat.sdi.client.sldmodel.client.element.SLDElement;
import org.ayat.sdi.client.sldmodel.client.util.SimpleList;

public interface DashArray extends SimpleList<Double>, SLDElement {
	String TAG_NAME = StringPool.SLD_ELEM_STROKE_ELEM_DASHARRAY_NAME;
}
