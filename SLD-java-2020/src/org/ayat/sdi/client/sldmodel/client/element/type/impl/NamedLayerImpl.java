package org.ayat.sdi.client.sldmodel.client.element.type.impl;

import static org.ayat.sdi.client.sldmodel.client.StringPool.SLD_ELEM_NAMEDSTYLE_NAME;

import org.ayat.sdi.client.sldmodel.client.element.SLDElementFactory;
import org.ayat.sdi.client.sldmodel.client.element.aspect.impl.SLDElementHasSelfDescriptionImpl;
import org.ayat.sdi.client.sldmodel.client.element.type.NamedLayer;
import org.ayat.sdi.client.sldmodel.client.element.type.UserStyle;
import org.ayat.sdi.client.sldmodel.client.util.ElementNotFoundException;
import org.ayat.sdi.client.sldmodel.client.util.UnknownElementTypeException;

import org.w3c.dom.*;

public class NamedLayerImpl extends SLDElementHasSelfDescriptionImpl implements
		NamedLayer {
	public NamedLayerImpl(Element element) {
		super(element);
	}

	public NamedLayerImpl(Document document) {
		super(document, NamedLayer.TAG_NAME);
	}

	@Override
	public String getNamedStyle() throws ElementNotFoundException {
		return getTextGrandChildValue(SLD_ELEM_NAMEDSTYLE_NAME);
	}

	@Override
	public void setNamedStyle(String namedStyle) {
		setTextGrandChildValue(SLD_ELEM_NAMEDSTYLE_NAME, namedStyle);
	}

	@Override
	public void setUserStyle(UserStyle userStyle) {
		setUniqueChild(userStyle.getElement());
	}

	@Override
	public UserStyle getUserStyle() throws ElementNotFoundException {
		try {
			return SLDElementFactory.getInstance(UserStyle.class,
					getUniqueChild(UserStyle.TAG_NAME));
		} catch (UnknownElementTypeException e) {// Will never happen
			throw new RuntimeException(e);
		}
	}

	@Override
	public UserStyle createUserStyle() {
		try {
			UserStyle result = SLDElementFactory.getInstance(UserStyle.class,
					document);
			setUserStyle(result);
			return result;
		} catch (UnknownElementTypeException e) {// Will never happen
			throw new RuntimeException(e);
		}
	}

	@Override
	public void removeUserStyle() {
		try {
			element.removeChild(getUserStyle().getElement());
		} catch (ElementNotFoundException e) {
			// Nothing to remove.
		}
	}
}
