package org.ayat.sdi.client.sldmodel.client.element.aspect;

import org.ayat.sdi.client.sldmodel.client.element.SLDElement;
import org.ayat.sdi.client.sldmodel.client.element.SLDElementList;
import org.ayat.sdi.client.sldmodel.client.element.type.FeatureTypeStyle;

import org.w3c.dom.*;

public interface SLDElementHasFeatureTypeStyleList extends SLDElement{
	void addFeatureTypeStyle(FeatureTypeStyle featureTypeStyle);

	void removeFeatureTypeStyle(FeatureTypeStyle featureTypeStyle);

	SLDElementList<FeatureTypeStyle> getFeatureTypeStyles();

	/**
	 * Creates a {@link FeatureTypeStyle} using the constructor
	 * {@link FeatureTypeStyle}({@link Document}) and passes it to
	 * {@link #addFeatureTypeStyle(FeatureTypeStyle)}.
	 * */
	FeatureTypeStyle createFeatureTypeStyle();
}
