package org.ayat.sdi.client.sldmodel.client.element.aspect;

import org.ayat.sdi.client.sldmodel.client.element.SLDElement;
import org.ayat.sdi.client.sldmodel.client.element.type.Fill;
import org.ayat.sdi.client.sldmodel.client.util.ElementNotFoundException;

import org.w3c.dom.*;

public interface SLDElementHasFill extends SLDElement {
	/**
	 * @throws ElementNotFoundException
	 */
	Fill getFill() throws ElementNotFoundException;

	void setFill(Fill value);

	/**
	 * Creates a {@link Fill} using the constructor {@link Fill}(
	 * {@link Document}) and passes it to {@link #setFill(Fill)}.
	 * */
	Fill createFill();

	/**
	 * If a {@link Fill} has been set before, removes it. Else does nothing.
	 * */
	void removeFill();
}
