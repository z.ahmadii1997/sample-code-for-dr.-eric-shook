package org.ayat.sdi.client.sldmodel.client.element.type;

import org.ayat.sdi.client.sldmodel.client.StringPool;
import org.ayat.sdi.client.sldmodel.client.element.SLDElement;
import org.ayat.sdi.client.sldmodel.client.element.aspect.SLDElementHasExternalGraphic;
import org.ayat.sdi.client.sldmodel.client.element.aspect.SLDElementHasMark;
import org.ayat.sdi.client.sldmodel.client.util.ElementNotFoundException;

public interface Graphic extends SLDElement, SLDElementHasExternalGraphic,
		SLDElementHasMark {
	String TAG_NAME = StringPool.SLD_ELEM_GRAPHIC_NAME;

	/**
	 * @throws ElementNotFoundException
	 * @throws NumberFormatException
	 */
	Double getOpacity() throws NumberFormatException, ElementNotFoundException;

	void setOpacity(double value);

	/**
	 * @throws ElementNotFoundException
	 * @throws NumberFormatException
	 */
	Integer getSize() throws NumberFormatException, ElementNotFoundException;

	void setSize(int value);

	/**
	 * @throws ElementNotFoundException
	 * @throws NumberFormatException
	 */
	Integer getRotation() throws NumberFormatException,
			ElementNotFoundException;

	void setRotation(int value);
}