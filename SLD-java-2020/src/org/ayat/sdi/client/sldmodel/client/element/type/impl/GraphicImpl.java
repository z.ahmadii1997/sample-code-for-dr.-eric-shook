package org.ayat.sdi.client.sldmodel.client.element.type.impl;

import org.ayat.sdi.client.sldmodel.client.StringPool;
import org.ayat.sdi.client.sldmodel.client.element.AbstractSLDElement;
import org.ayat.sdi.client.sldmodel.client.element.SLDElementFactory;
import org.ayat.sdi.client.sldmodel.client.element.type.ExternalGraphic;
import org.ayat.sdi.client.sldmodel.client.element.type.Graphic;
import org.ayat.sdi.client.sldmodel.client.element.type.Mark;
import org.ayat.sdi.client.sldmodel.client.util.ElementNotFoundException;
import org.ayat.sdi.client.sldmodel.client.util.UnknownElementTypeException;

import org.w3c.dom.*;

public class GraphicImpl extends AbstractSLDElement implements Graphic {
	public GraphicImpl(Document document) {
		super(document, Graphic.TAG_NAME);
	}

	public GraphicImpl(Element element) {
		super(element);
	}

	@Override
	public ExternalGraphic getExternalGraphic() throws ElementNotFoundException {
		try {
			return SLDElementFactory.getInstance(ExternalGraphic.class,
					getUniqueChild(ExternalGraphic.TAG_NAME));
		} catch (UnknownElementTypeException e) {// Will never happen
			throw new RuntimeException(e);
		}
	}

	@Override
	public Mark getMark() throws ElementNotFoundException {
		try {
			return SLDElementFactory.getInstance(Mark.class,
					getUniqueChild(Mark.TAG_NAME));
		} catch (UnknownElementTypeException e) {// Will never happen
			throw new RuntimeException(e);
		}
	}

	@Override
	public Double getOpacity() throws NumberFormatException,
			ElementNotFoundException {
		return getDoubleGrandChildValue(StringPool.SLD_ELEM_ANY_ELEM_OPACITY_NAME);
	}

	@Override
	public Integer getRotation() throws NumberFormatException,
			ElementNotFoundException {
		return getIntegerGrandChildValue(StringPool.SLD_ELEM_GRAPHIC_ELEM_ROTATION_NAME);
	}

	@Override
	public Integer getSize() throws NumberFormatException,
			ElementNotFoundException {
		return getIntegerGrandChildValue(StringPool.SLD_ELEM_GRAPHIC_ELEM_SIZE_NAME);
	}

	@Override
	public void setExternalGraphic(ExternalGraphic value) {
		setUniqueChild(value.getElement());
	}

	@Override
	public void setMark(Mark value) {
		setUniqueChild(value.getElement());
	}

	@Override
	public void setOpacity(double value) {
		setDoubleGrandChildValue(StringPool.SLD_ELEM_ANY_ELEM_OPACITY_NAME,
				value);
	}

	@Override
	public void setRotation(int value) {
		setIntegerGrandChildValue(
				StringPool.SLD_ELEM_GRAPHIC_ELEM_ROTATION_NAME, value);
	}

	@Override
	public void setSize(int value) {
		setIntegerGrandChildValue(StringPool.SLD_ELEM_GRAPHIC_ELEM_SIZE_NAME,
				value);
	}

	@Override
	public ExternalGraphic createExternalGraphic() {
		try {
			ExternalGraphic result = SLDElementFactory.getInstance(
					ExternalGraphic.class, document);
			setExternalGraphic(result);
			return result;
		} catch (UnknownElementTypeException e) {// Will never happen
			throw new RuntimeException(e);
		}
	}

	@Override
	public Mark createMark() {
		try {
			Mark result = SLDElementFactory.getInstance(Mark.class, document);
			setMark(result);
			return result;
		} catch (UnknownElementTypeException e) {// Will never happen
			throw new RuntimeException(e);
		}
	}

	@Override
	public void removeExternalGraphic() {
		try {
			element.removeChild(getExternalGraphic().getElement());
		} catch (ElementNotFoundException e) {
			//Nothing to remove.
		}
	}

	@Override
	public void removeMark() {
		try {
			element.removeChild(getMark().getElement());
		} catch (ElementNotFoundException e) {
			//Nothing to remove.
		}
	}
}
