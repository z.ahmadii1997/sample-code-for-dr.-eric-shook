package org.ayat.sdi.client.sldmodel.client.element.type.impl;

import org.ayat.sdi.client.sldmodel.client.element.aspect.impl.SLDElementHasGraphicImpl;
import org.ayat.sdi.client.sldmodel.client.element.type.PointSymbolizer;

import org.w3c.dom.*;

public class PointSymbolizerImpl extends SLDElementHasGraphicImpl
		implements PointSymbolizer {
	public PointSymbolizerImpl(Document document) {
		super(document, PointSymbolizer.TAG_NAME);
	}

	public PointSymbolizerImpl(Element element) {
		super(element);
	}
}
